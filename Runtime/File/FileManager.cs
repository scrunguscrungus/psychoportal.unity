﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

namespace PsychoPortal.Unity
{
    /// <summary>
    /// Manages file system and packaged files for the game
    /// </summary>
    public class FileManager : IDisposable
    {
        public FileManager(IFileSystemService fileSystem, PsychonautsSettings settings, string workingDirectory)
        {
            FileSystem = fileSystem;
            Settings = settings;
            WorkingDirectory = workingDirectory;
        }

        private const string _workResource = "WorkResource";
        private const string _workResourcePS2 = "WorkPS2";

        private Stream _packageStream;
        private Stream[] _ps2ResourcePacks;

        public IFileSystemService FileSystem { get; }
        public PsychonautsSettings Settings { get; }
        public PsychonautsVersion Version => Settings.Version;
        public string WorkingDirectory { get; }

        // Package
        private string PackageStreamFilePath { get; set; }
        private Stream PackageStream
        {
            get
            {
                if (_packageStream == null && PackageStreamFilePath != null)
                {
                    _packageStream = FileSystem.OpenFile(PackageStreamFilePath, FileAccess.Read);
                    Debug.Log("Opened package file stream");
                }

                return _packageStream;
            }
            set => _packageStream = value;
        }
        private Dictionary<string, FileRef> PackagedFiles { get; set; }

        // PS2 files
        public PS2_FileTable PS2_FileTable { get; private set; }
        private string[] PS2_ResourcePackFilePaths { get; set; }
        public Stream[] PS2_ResourcePacks
        {
            get
            {
                if (_ps2ResourcePacks == null && PS2_ResourcePackFilePaths != null)
                {
                    PS2_ResourcePacks = PS2_ResourcePackFilePaths.
                        Where(x => FileSystem.FileExists(GetPhysicalAbsolutePath(x, false))).
                        Select(x => FileSystem.OpenFile(GetPhysicalAbsolutePath(x, false), FileAccess.Read)).
                        ToArray();
                    Debug.Log("Opened resource pack file streams");
                }

                return _ps2ResourcePacks;
            }
            private set => _ps2ResourcePacks = value;
        }

        private Dictionary<uint, PS2_FileEntry> PS2_Files { get; set; }

        private string GetFileLogName(string filePath) => Path.GetFileName(filePath);


        private string GetPhysicalAbsolutePath(string path = null, bool inWorkResource = true)
        {
            if (inWorkResource)
                return Path.Combine(WorkingDirectory, GetWorkresourcePath(path));
            else
                return path == null ? path : Path.Combine(WorkingDirectory, path);
        }

        private string GetWorkresourcePath(string filePath = null)
        {
            string root = Version == PsychonautsVersion.PS2 ? _workResourcePS2 : _workResource;
            return filePath == null ? root : Path.Combine(root, filePath);
        }

        private string NormalizePackagedPath(string path) => path.Replace('\\', '/').ToLower();

        public void LoadPackage(string filePath, IBinarySerializerLogger logger = null)
        {
            if (Version == PsychonautsVersion.PS2)
                throw new InvalidOperationException("A package can't be loaded for the PS2 version");
            
            UnloadPackage(true);

            PackageStreamFilePath = GetPhysicalAbsolutePath(filePath, false);
            PackagedFiles = new Dictionary<string, FileRef>();

            if (Version == PsychonautsVersion.Xbox_Proto_20041217)
            {
                ScratchInstallation drive = Binary.ReadFromStream<ScratchInstallation>(PackageStream, Settings, logger: logger, name: GetFileLogName(filePath));

                foreach (ScratchInstallationDirectory dir in drive.Directories)
                {
                    foreach (ScratchInstallationFile file in dir.Files)
                    {
                        string fullPath = $"{dir.Name}/{file.Name}";
                        PackagedFiles[NormalizePackagedPath(fullPath)] = new FileRef(fullPath, (uint)file.FileDataOffset, file.FileSize);
                    }
                }
            }
            else
            {
                Package package = Binary.ReadFromStream<Package>(PackageStream, Settings, logger: logger, name: GetFileLogName(filePath));

                foreach (var file in package.GetFiles())
                {
                    PackagedFiles[NormalizePackagedPath(file.FilePath)] = new FileRef(file.FilePath, file.FileEntry.FileDataOffset, file.FileEntry.FileSize);
                }
            }
        }

        public void UnloadPackage(bool fullyUnload)
        {
            PackageStream?.Dispose();
            PackageStream = null;

            if (fullyUnload)
            {
                PackageStreamFilePath = null;
                PackagedFiles = null;
            }
        }

        public void PS2_LoadResourcePacks(string[] resourceFilePaths, IBinarySerializerLogger logger = null)
        {
            PS2_UnloadResourcePacks(true);

            if (resourceFilePaths.Length == 0)
                return;

            PS2_ResourcePackFilePaths = resourceFilePaths;

            // Read the file table
            PS2_FileTable = Binary.ReadFromStream<PS2_FileTable>(PS2_ResourcePacks[0], Settings, logger: logger, name: GetFileLogName(resourceFilePaths[0]));

            PS2_Files = new Dictionary<uint, PS2_FileEntry>();

            foreach (PS2_FileEntry file in PS2_FileTable.Files)
                PS2_Files[file.FilePathHash] = file;
        }

        public void PS2_UnloadResourcePacks(bool fullyUnload)
        {
            if (PS2_ResourcePacks != null)
            {
                foreach (Stream stream in PS2_ResourcePacks)
                    stream?.Dispose();

                PS2_ResourcePacks = null;
            }

            if (fullyUnload)
            {
                PS2_ResourcePackFilePaths = null;
                PS2_FileTable = null;
                PS2_Files = null;
            }
        }

        public bool FileExists(FileRef fileRef)
        {
            // Normalize the file path root
            string filePath = fileRef.GetNormalizedFilePathRoot();

            // The PS2 version has all the files in resource packs
            if (Version == PsychonautsVersion.PS2)
            {
                uint hash;

                // Hack for checking unnamed files using their hash
                if (filePath.StartsWith("0x"))
                {
                    hash = UInt32.Parse(filePath[2..], NumberStyles.HexNumber);
                }
                else
                {
                    filePath = GetWorkresourcePath(filePath);

                    // Get the file path hash
                    hash = PS2_FileEntry.GetFilePathHash(filePath);
                }

                return PS2_Files.ContainsKey(hash);
            }

            // Check the package
            if (fileRef.Location.HasFlag(FileLocation.Package) && PackagedFiles.ContainsKey(NormalizePackagedPath(filePath)))
                return true;

            // Check the file system
            if (fileRef.Location.HasFlag(FileLocation.FileSystem) && FileSystem.FileExists(GetPhysicalAbsolutePath(filePath)))
                return true;

            return false;
        }

        public Stream OpenFile(FileRef fileRef, bool throwIfNotFound = false)
        {
            // Normalize the file path
            string filePath = fileRef.GetNormalizedFilePathRoot();

            // The PS2 version has all the files in resource packs
            if (Version == PsychonautsVersion.PS2)
            {
                uint hash;

                // Hack for loading unnamed files from their hash
                if (filePath.StartsWith("0x"))
                {
                    hash = UInt32.Parse(filePath[2..], NumberStyles.HexNumber);
                }
                else
                {
                    filePath = GetWorkresourcePath(filePath);

                    // Get the file path hash
                    hash = PS2_FileEntry.GetFilePathHash(filePath);
                }

                PS2_FileEntry pakFileEntry = PS2_Files.TryGetValue(hash, out PS2_FileEntry e) ? e : null;

                if (pakFileEntry != null)
                    return new MemoryStream(pakFileEntry.ReadFile(PS2_ResourcePacks));
            }
            else
            {
                // Check the master package
                if (fileRef.Location.HasFlag(FileLocation.Package))
                {
                    if (!fileRef.HasFileOffset || !fileRef.HasFileSize)
                    {
                        FileRef? r = PackagedFiles.TryGetValue(NormalizePackagedPath(filePath), out FileRef rr) ? rr : null;

                        if (r != null)
                            fileRef = r.Value;
                    }

                    if (fileRef.HasFileOffset && fileRef.HasFileSize)
                    {
                        byte[] buffer = new byte[fileRef.FileSize];
                        PackageStream.Position = fileRef.FileOffset;
                        int read = PackageStream.Read(buffer, 0, (int)fileRef.FileSize);

                        if (read != fileRef.FileSize)
                            throw new EndOfStreamException();

                        return new MemoryStream(buffer);
                    }
                }

                // Check the file system
                if (fileRef.Location.HasFlag(FileLocation.FileSystem))
                {
                    // Fall back to a physical file if one exists
                    string physicalPath = GetPhysicalAbsolutePath(filePath);

                    if (FileSystem.FileExists(physicalPath))
                        return FileSystem.OpenFile(physicalPath, FileAccess.Read);
                }
            }

            // Return null or throw if not found
            if (throwIfNotFound)
                throw new FileNotFoundException($"The file {filePath} could not be found", filePath);
            else
                return null;
        }

        public T ReadFromFile<T>(FileRef fileRef, IBinarySerializerLogger logger = null, bool throwIfNotFound = false, bool logIfNotFullyRead = true, Action<IBinarySerializer, T> onPreSerializing = null)
            where T : IBinarySerializable, new()
        {
            using Stream file = OpenFile(fileRef, throwIfNotFound);

            if (file == null)
                return default;

            T obj;

            try
            {
                obj = Binary.ReadFromStream<T>(file, Settings, onPreSerializing: onPreSerializing, logger: logger, name: fileRef.FilePath);
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to deserialize file {fileRef.FilePath}", ex);
            }

            if (logIfNotFullyRead && file.Position < file.Length)
                Debug.LogWarning($"File {fileRef.FilePath} was not fully read");

            return obj;
        }

        public IEnumerable<FileRef> EnumerateFiles(FileLocation location, bool includeUnnamedPS2Files = false)
        {
            if (Version == PsychonautsVersion.PS2)
            {
                HashSet<uint> namedFiles = null;

                if (includeUnnamedPS2Files)
                    namedFiles = new HashSet<uint>();

                foreach (string filePath in PS2_FileNames.FileTable)
                {
                    string path = filePath;

                    // Get the file path hash
                    uint hash = PS2_FileEntry.GetFilePathHash(path);

                    // Make sure the file exists. The provided file table contains files from all known released of the game, all of
                    // which don't exist in all versions.
                    bool exists = PS2_Files.ContainsKey(hash);

                    if (!exists)
                        continue;

                    if (includeUnnamedPS2Files)
                        namedFiles.Add(hash);

                    // Remove workps2 from the file paths
                    int rootCharIndex = path.IndexOf(_workResourcePS2, StringComparison.InvariantCultureIgnoreCase);

                    if (rootCharIndex != -1 &&
                        (path[rootCharIndex + _workResourcePS2.Length] == '/' || path[rootCharIndex + _workResourcePS2.Length] == '\\'))
                        path = path.Substring(rootCharIndex + _workResourcePS2.Length + 1);

                    yield return path;
                }

                if (includeUnnamedPS2Files)
                    foreach (PS2_FileEntry fileEntry in PS2_FileTable.Files.Where(x => !namedFiles.Contains(x.FilePathHash)))
                        yield return $"0x{fileEntry.FilePathHash:X8}";

                yield break;
            }

            if (location.HasFlag(FileLocation.Package) && PackagedFiles != null)
                foreach (FileRef fileRef in PackagedFiles.Values)
                    yield return fileRef;

            if (location.HasFlag(FileLocation.FileSystem))
            {
                string basePath = GetPhysicalAbsolutePath(inWorkResource: true);

                foreach (string file in FileSystem.EnumerateFiles(basePath, "*", SearchOption.AllDirectories))
                    yield return file[(basePath.Length + 1)..];
            }
        }

        public void ExportPackagedFiles(string outputDir)
        {
            if (Version == PsychonautsVersion.PS2)
            {
                PS2_FileTable?.ExportFiles(outputDir, Array.Empty<string>(), PS2_ResourcePacks);
            }
            else if (PackagedFiles != null)
            {
                foreach (FileRef file in PackagedFiles.Values)
                {
                    byte[] fileBuffer = new byte[file.FileSize];
                    PackageStream.Position = file.FileOffset;
                    int read = PackageStream.Read(fileBuffer, 0, fileBuffer.Length);

                    if (read != fileBuffer.Length)
                        throw new EndOfStreamException();

                    // TODO: Move to file system service?
                    string outputFilePath = Path.Combine(outputDir, file.FilePath);
                    Directory.CreateDirectory(Path.GetDirectoryName(outputFilePath));
                    File.WriteAllBytes(outputFilePath, fileBuffer);
                }
            }
        }

        public void Dispose()
        {
            UnloadPackage(false);
            PS2_UnloadResourcePacks(false);
         
            Debug.Log("Disposed file manager");
        }
    }
}