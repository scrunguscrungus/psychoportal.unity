﻿using System;
using System.Linq;
using UnityEngine;

namespace PsychoPortal.Unity
{
    public class SkeletonAnimationComponent : MonoBehaviour
    {
        private string _prevAnimFile;
        private int _animLength;
        private PsychonautsSkelAnim _anim;
        private JointID _rootJointID;

        public AnimationManager AnimationManager;
        public FileManager FileManager;
        public IBinarySerializerLogger Logger;
        public string AnimFile;
        public PsychonautsMesh Mesh;
        public PsychonautsSettings Settings;

        public float AnimFrame;
        public bool Animate = true;
        public float AnimSpeed = 1;
        public float FrameRate = 30;

        public PsychonautsSkelAnim Anim => _anim;
        public int AnimLength => _animLength;

        private void Update()
        {
            if (AnimFile != _prevAnimFile)
            {
                _anim = AnimationManager?.GetAnimation(AnimFile);
                _prevAnimFile = AnimFile;

                if (_anim == null)
                {
                    ResetAnimation();

                    if (!String.IsNullOrWhiteSpace(AnimFile))
                        Debug.LogError("Animation file not found");
                    
                    return;
                }

                InitAnim();
            }

            // Make sure an animation has been set
            if (_anim == null)
                return;

            // Update the animation frame
            if (Animate)
                AnimFrame = (AnimFrame + Time.deltaTime * FrameRate * AnimSpeed) % _animLength;
            
            // Animate the joints
            for (int i = 0; i < _anim.JointAnimations.Length; i++)
            {
                // Get the joint
                PsychonautsJoint joint = Mesh.Skeletons[_rootJointID.SkeletonIndex].Joints[_rootJointID.JointIndex + i];

                // Process every channel
                foreach (SharedJointAnim_Channel channel in _anim.JointAnimations[i].Channels)
                    AnimateJointChannel(joint, channel);
            }

            if (_anim.BlendAnimation != null)
            {
                foreach (SharedBlendAnim_Channel channel in _anim.BlendAnimation.Channels)
                    AnimateBlendChannel(channel);
            }
        }

        private void InitAnim()
        {
            ResetAnimation();

            try
            {
                using (FileManager)
                {
                    using (Logger)
                    {
                        _anim.Init(FileManager, Logger);
                    }
                }
            }
            catch (Exception ex)
            {
                AnimError($"Animation failed to initialize: {ex.Message}");
                return;
            }

            JointID rootJointID = Mesh.GetJointID(_anim.RootJointName);

            if (rootJointID == null)
            {
                AnimError($"Animation root joint {_anim.RootJointName} does not exist in mesh {Mesh.Mesh.Name}");
                return;
            }

            PsychonautsSkeleton skel = Mesh.Skeletons[rootJointID.SkeletonIndex];

            if (rootJointID.JointIndex == 0 && skel.Joints.Length != _anim.JointAnimations.Length ||
                rootJointID.JointIndex != 0 && skel.Joints.Length < rootJointID.JointIndex + _anim.JointAnimations.Length)
            {
                AnimError($"Animation joints don't match in skeleton {skel.Skeleton.Name}");
                return;
            }

            _rootJointID = rootJointID;

            if (_anim.Header.Version < 201)
                _animLength = (int)_anim.JointAnimations[0].Channels.Max(x => x.KeyFrames.Last().Time);
            else
                _animLength = _anim.Header.V201_AnimLength;

            if (_anim.BlendAnimation != null)
            {
                foreach (SharedBlendAnim_Channel channel in _anim.BlendAnimation.Channels)
                {
                    if (Mesh.MeshFrags.All(x => x.MeshFrag.BlendshapeData?.Streams.Any(s => s.BlendMappingName == channel.Name) != true))
                    {
                        AnimError($"Animation blend shape {channel.Name} doesn't exist in the mesh");
                        return;
                    }
                }
            }
        }

        private void ResetAnimation()
        {
            AnimFrame = 0;

            if (_rootJointID != null)
                Mesh?.Skeletons?.ElementAtOrDefault(_rootJointID.SkeletonIndex)?.ResetTransforms();

            _rootJointID = null;

            if (Mesh != null)
                foreach (PsychonautsMeshFrag meshFrag in Mesh.MeshFrags)
                    meshFrag.BlendComponent?.Reset();
        }

        private void AnimError(string errorLog)
        {
            ResetAnimation();
            Debug.LogError(errorLog);
            _anim = null;
        }

        private void AnimateJointChannel(PsychonautsJoint joint, SharedJointAnim_Channel channel)
        {
            int keyFrameIndex;

            // Find the current key frame index we're at
            for (keyFrameIndex = 0; keyFrameIndex < channel.KeyFrames.Length; keyFrameIndex++)
            {
                if (channel.KeyFrames[keyFrameIndex].Time >= AnimFrame)
                    break;
            }

            // If we've exceeded the keyframes we always want to use the last frame
            if (keyFrameIndex == channel.KeyFrames.Length)
                keyFrameIndex = channel.KeyFrames.Length - 1;

            // Get the two key frames to blend
            SharedJointAnim_KeyFrame fromFrame = channel.KeyFrames[Math.Max(keyFrameIndex - 1, 0)];
            SharedJointAnim_KeyFrame toFrame = channel.KeyFrames[keyFrameIndex];

            // Calculate the ratio to blend by
            uint framesDifference = toFrame.Time - fromFrame.Time;
            float lerpRatio = framesDifference == 0 ? 1 : (AnimFrame - fromFrame.Time) / framesDifference;

            // TODO: Optimize this by caching the converted value types (to avoid doing things like ToVector3() each frame)
            switch (channel.Type)
            {
                case SharedJointAnim_ChannelType.Position:
                    joint.Transform.localPosition = Vector3.Lerp(
                        fromFrame.Position.ToVector3(), 
                        toFrame.Position.ToVector3(), 
                        lerpRatio);
                    break;

                case SharedJointAnim_ChannelType.Rotation when Settings.Version != PsychonautsVersion.PS2:
                    joint.Transform.localRotation = Quaternion.Slerp(
                        fromFrame.Rotation.ToQuaternion(),
                        toFrame.Rotation.ToQuaternion(),
                        lerpRatio);
                    break;

                case SharedJointAnim_ChannelType.Rotation when Settings.Version == PsychonautsVersion.PS2:
                    joint.Transform.localRotation = Quaternion.Slerp(
                        fromFrame.PS2_Rotation.ToQuaternion(),
                        toFrame.PS2_Rotation.ToQuaternion(),
                        lerpRatio);
                    break;

                case SharedJointAnim_ChannelType.Scale:
                    joint.Transform.localScale = Vector3.Lerp(
                        fromFrame.Scale.ToVector3(), 
                        toFrame.Scale.ToVector3(), 
                        lerpRatio);
                    break;

                case SharedJointAnim_ChannelType.CompressedRotation:
                    joint.Transform.localRotation = Quaternion.Slerp(
                        fromFrame.CompressedRotation.ToQuaternion(), 
                        toFrame.CompressedRotation.ToQuaternion(), 
                        lerpRatio);
                    break;
            }
        }

        private void AnimateBlendChannel(SharedBlendAnim_Channel channel)
        {
            string blendName = channel.Name;

            int currentFrameIndex = ((int)AnimFrame % channel.AnimData.Length);
            int nextFrameIndex = (currentFrameIndex + 1) % channel.AnimData.Length;

            int framesDiff = channel.AnimData[nextFrameIndex] - channel.AnimData[currentFrameIndex];
            float lerpRatio = AnimFrame - (int)AnimFrame;

            float valueInterpolated = (channel.AnimData[currentFrameIndex] + framesDiff * lerpRatio) / 255f;

            foreach (MeshFragBlendComponent fragBlend in Mesh.MeshFrags.Select(x => x.BlendComponent).Where(x => x != null))
            {
                for (int i = 0; i < fragBlend.BlendshapeStreams.Length; i++)
                {
                    if (blendName != fragBlend.BlendshapeStreams[i].BlendMappingName)
                        continue;

                    fragBlend.BlendWeights[i] = valueInterpolated;
                }
            }
        }
    }
}