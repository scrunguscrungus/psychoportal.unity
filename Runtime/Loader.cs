﻿using System;
using System.IO;

namespace PsychoPortal.Unity
{
    /// <summary>
    /// Manages loading data from the game
    /// </summary>
    public class Loader : IDisposable
    {
        #region Constructor

        public Loader(PsychonautsSettings settings, string directory) : this(new FileSystemService(), settings, directory)
        { }

        public Loader(IFileSystemService fileSystem, PsychonautsSettings settings, string directory)
        {
            Settings = settings;
            Directory = directory;

            FileManager = new FileManager(fileSystem, settings, directory);
            TexturesManager = new TexturesManager(Settings, FileManager);
            AnimationManager = new AnimationManager();
        }

        #endregion

        #region Public Properties

        public PsychonautsSettings Settings { get; }
        public PsychonautsVersion Version => Settings.Version;
        public string Directory { get; }
        public TexturesManager TexturesManager { get; }
        public AnimationManager AnimationManager { get; }
        public FileManager FileManager { get; }

        // Configuration
        public GameLanguage Language { get; set; } = GameLanguage.English;
        public bool UseNativeTextures { get; set; } = true;

        // Common
        public TexturePack CommonTexturePack { get; private set; }
        public MeshPack CommonMeshPack { get; private set; }
        public ScriptPack CommonScriptPack { get; private set; }
        public AnimPack CommonAnimPack { get; private set; }

        // Level
        public TexturePack LevelTexturePack { get; private set; }
        public MeshPack LevelMeshPack { get; private set; }
        public ScriptPack LevelScriptPack { get; private set; }
        public Scene LevelScene { get; private set; }
        public Scene[] ReferencedLevelScenes { get; private set; }
        public AnimPack LevelAnimPack { get; private set; }

        #endregion

        #region Path Methods

        public string GetLevelPacksDirectoryPath()
        {
            if (Version == PsychonautsVersion.PC_Digital)
                return "PCLevelPackFiles";
            else
                return "LevelPackFiles";
        }
        public string GetCommonPackPackFilePath() => GetPackPackFilePath("common");
        public string GetCommonAnimPackFilePath() => GetAnimPackFilePath("common");
        public string GetPackPackFilePath(string lvl) => Path.Combine(GetLevelPacksDirectoryPath(), $"{lvl}.ppf");
        public string GetAnimPackFilePath(string lvl) => Path.Combine(GetLevelPacksDirectoryPath(), $"{lvl}.apf");
        public string GetMasterPackageFilePath() => Version switch
        {
            PsychonautsVersion.PC_Digital => "Psychonautsdata2.pkg",
            PsychonautsVersion.Xbox_Proto_20041217 => "scratchdrive.bin",
            PsychonautsVersion.Xbox_Proto_20050214 => "PsychonautsData0.pkg",
            PsychonautsVersion.PS2 => throw new Exception("The PS2 version doesn't have a master package"),
            _ => throw new NotImplementedException()
        };
        public string[] PS2_GetResourcePackFilePaths() => new string[]
        {
            "RESOURCE.PAK",
            "RESOURC2.PAK",
        };

        #endregion

        #region Pack Load Methods

        /// <summary>
        /// Loads the file packages used by the current version of the game. On PS2 this is required before attempting to read other files.
        /// </summary>
        /// <param name="logger">An optional logger for the serialization</param>
        public void LoadFilePackages(IBinarySerializerLogger logger = null)
        {
            if (Version == PsychonautsVersion.PS2)
            {
                // Load the resource packs
                FileManager.PS2_LoadResourcePacks(PS2_GetResourcePackFilePaths(), logger);
            }
            else
            {
                // Load the package
                FileManager.LoadPackage(GetMasterPackageFilePath(), logger);
            }
        }

        public void LoadCommonPackPack(IBinarySerializerLogger logger = null)
        {
            // Unload previously loaded common textures
            TexturesManager.UnloadPackedTextures(CommonTexturePack);

            // Read the pack
            PackPack ppf = FileManager.ReadFromFile<CommonPackPack>(new FileRef(GetCommonPackPackFilePath(), FileLocation.FileSystem), logger: logger);

            // Get the packs
            CommonTexturePack = ppf.TexturePack;
            CommonMeshPack = ppf.MeshPack;
            CommonScriptPack = ppf.ScriptPack;

            // Load the textures
            TexturesManager.LoadPackedTextures(CommonTexturePack, UseNativeTextures, Language);
        }

        public void LoadCommonAnimPack(IBinarySerializerLogger logger = null)
        {
            // Read the pack
            CommonAnimPack = FileManager.ReadFromFile<AnimPack>(new FileRef(GetCommonAnimPackFilePath(), FileLocation.FileSystem), logger: logger);
        }

        public void LoadLevelPackPack(string level, IBinarySerializerLogger logger = null)
        {
            // Unload previously loaded level textures
            TexturesManager.UnloadPackedTextures(LevelTexturePack);

            // Read the pack
            PackPack ppf = FileManager.ReadFromFile<PackPack>(new FileRef(GetPackPackFilePath(level), FileLocation.FileSystem), logger: logger);

            // Get the packs
            LevelTexturePack = ppf.TexturePack;
            LevelMeshPack = ppf.MeshPack;
            LevelScriptPack = ppf.ScriptPack;
            LevelScene = ppf.Scene;
            ReferencedLevelScenes = ppf.ReferencedScenes;

            // Load the textures
            TexturesManager.LoadPackedTextures(LevelTexturePack, UseNativeTextures, Language);
        }

        public void LoadLevelAnimPack(string level, IBinarySerializerLogger logger = null)
        {
            // Read the pack
            LevelAnimPack = FileManager.ReadFromFile<AnimPack>(new FileRef(GetAnimPackFilePath(level), FileLocation.FileSystem), logger: logger);
        }

        #endregion

        #region General Methods

        public virtual void Dispose()
        {
            FileManager?.Dispose();
        }

        #endregion
    }
}