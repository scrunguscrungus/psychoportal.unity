﻿using System;
using System.Linq;
using UnityEngine;

namespace PsychoPortal.Unity
{
    public class PsychonautsSkeleton
    {
        #region Constructors

        public PsychonautsSkeleton(Skeleton skeleton, PsychonautsSettings settings = null)
        {
            Skeleton = skeleton;
            Joints = new PsychonautsJoint[skeleton.JointsCount];

            addJoint(skeleton.RootJoint, null);

            ResetTransforms();

            void addJoint(Joint j, Transform parentJoint)
            {
                GameObject jointObj = new GameObject($"Joint_{j.ID.JointIndex}: {j.Name}");

                if (parentJoint != null)
                    jointObj.transform.SetParent(parentJoint);

                if (settings != null)
                    jointObj.AddBinarySerializableData(settings, j);

                Joints[j.ID.JointIndex] = new PsychonautsJoint(j, jointObj);

                foreach (Joint child in j.Children)
                    addJoint(child, jointObj.transform);
            }
        }

        public PsychonautsSkeleton(Skeleton skeleton, Transform parent, PsychonautsSettings settings = null) : this(skeleton, settings)
        {
            RootJoint.Transform.SetParent(parent);
        }

        public PsychonautsSkeleton(Skeleton skeleton, PsychonautsJoint[] joints)
        {
            Skeleton = skeleton;
            Joints = joints;
        }

        #endregion

        #region Public Properties

        public Skeleton Skeleton { get; }
        public PsychonautsJoint[] Joints { get; }
        public PsychonautsJoint RootJoint => Joints.FirstOrDefault();

        #endregion

        #region Public Methods

        public PsychonautsJoint GetJoint(string name) => Joints.FirstOrDefault(x => x.Joint.Name == name);
        public int GetJointIndex(string name) => Array.FindIndex(Joints, x => x.Joint.Name == name);

        public void ResetTransforms()
        {
            foreach (PsychonautsJoint joint in Joints)
                joint.ResetTransform();
        }

        #endregion
    }
}