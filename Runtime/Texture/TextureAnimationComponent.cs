﻿using System;
using UnityEngine;

namespace PsychoPortal.Unity
{
    public class TextureAnimationComponent : MonoBehaviour
    {
        public PsychonautsTexture Texture;
        public TextureReference TextureRef;
        public Material Material;
        public string TextureName = "_MainTex";

        public int CurrentFrame;
        public AnimPlayMode PlayMode;
        public bool IsAnimatingForward = true;

        private bool UpdateAnimation(float totalTimeMS)
        {
            if (Texture == null)
                return false;

            TexAnimInfo anim = Texture.AnimInfo;

            totalTimeMS -= anim.Delay;

            if (totalTimeMS < 0)
                totalTimeMS = 0;

            if (anim.FramesPerSecond == 0)
                return false;

            int framesCount = anim.FramesCount;
            float fps = anim.FramesPerSecond;

            int prevFrame = CurrentFrame;

            switch (PlayMode)
            {
                case AnimPlayMode.Loop:
                {
                    float totalAnimTimeMS = framesCount * 1000f / fps;
                    float timeInAnim = totalTimeMS % totalAnimTimeMS;
                    CurrentFrame = Mathf.FloorToInt(framesCount * timeInAnim / totalAnimTimeMS);
                    IsAnimatingForward = true;
                }
                    break;

                case AnimPlayMode.LoopOnce:
                {
                    if (framesCount - 1 <= CurrentFrame)
                    {
                        PlayMode = AnimPlayMode.Stop;
                        return false;
                    }

                    float totalAnimTimeMS = framesCount * 1000f / fps;
                    float timeInAnim = Math.Min(totalTimeMS, totalAnimTimeMS - 1);
                    CurrentFrame = Mathf.FloorToInt(framesCount * timeInAnim / totalAnimTimeMS);
                    IsAnimatingForward = true;
                }
                    break;

                case AnimPlayMode.LoopTail:
                {
                    float totalAnimTimeMS = framesCount * 1000f / fps;
                    float loop = (anim.LoopFrame * 1000f) / fps;

                    float timeInAnim;

                    if (totalTimeMS <= loop)
                        timeInAnim = totalTimeMS;
                    else
                        timeInAnim = (totalTimeMS - loop) % (totalAnimTimeMS - loop) + loop;

                    CurrentFrame = Mathf.FloorToInt(framesCount * timeInAnim / totalAnimTimeMS);
                    IsAnimatingForward = true;
                }
                    break;

                case AnimPlayMode.Oscillate:
                case AnimPlayMode.OscillateOnce:
                case AnimPlayMode.OscillateOutOnce:
                case AnimPlayMode.OscillateBackOnce:
                    throw new NotImplementedException($"Not implemented {PlayMode} animations");

                case AnimPlayMode.Stop:
                default:
                    // Do nothing
                    break;
            }

            return prevFrame != CurrentFrame;
        }

        private void Update()
        {
            bool newTex = UpdateAnimation((float)(Time.timeAsDouble * 1000));

            if (newTex)
                Material.SetTexture(TextureName, Texture.GetTextureFrames(TextureRef.RepeatUV)[CurrentFrame]);
        }
        
        public void SetTexture(PsychonautsTexture texture, TextureReference textureRef, Material material, string textureName = "_MainTex")
        {
            Texture = texture;
            TextureRef = textureRef;
            CurrentFrame = (int?)Texture.AnimInfo?.CurrentFrame ?? 0;
            PlayMode = Texture.AnimInfo?.PlayMode ?? AnimPlayMode.Stop;
            Material = material;
            TextureName = textureName;
        }
    }
}