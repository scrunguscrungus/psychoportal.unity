﻿using UnityEngine;

namespace PsychoPortal.Unity
{
    public static class PS2_TextureExtensions
    {
        public static Texture2D ToTexture2D(this PS2_Texture ps2, bool flipY = false)
        {
            // TODO: Optimize this by setting format to 24-bit if the PS2 texture has no alpha
            // Create the texture
            Texture2D tex = new((int)ps2.Width, (int)ps2.Height, UnityEngine.TextureFormat.RGBA32, false);

            // Convert and untile palette
            Color[] pal = TextureHelpers.UntilePS2Palette(TextureHelpers.GetPS2Palette_32(ps2.Palette));
            
            // Set pixels
            PS2_PixelStorageMode pixelMode = ps2.Format.GetTexturePixelStorageMode();
            Color[] pixels = TextureHelpers.GetPS2PixelColors(pixelMode, ps2.ImgData, pal, (int)ps2.Width, (int)ps2.Height, ps2.IsSwizzled, flipY: flipY);
            tex.SetPixels(pixels);
            tex.Apply();

            return tex;
        }
    }
}