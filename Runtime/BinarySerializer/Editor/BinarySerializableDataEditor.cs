﻿using UnityEditor;

namespace PsychoPortal.Unity
{
    [CustomEditor(typeof(BinarySerializableDataComponent))]
    [CanEditMultipleObjects]
    public class BinarySerializableDataEditor : Editor
    {
        private InspectorSerializer _serializer;
        private bool _dataFoldout = true;

        public override void OnInspectorGUI()
        {
            BinarySerializableDataComponent comp = (BinarySerializableDataComponent)serializedObject.targetObject;

            _dataFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(_dataFoldout, comp.Data.GetType().Name);

            EditorGUI.indentLevel++;

            if (_dataFoldout)
                comp.Data.Serialize(_serializer ??= new InspectorSerializer(comp.Settings));

            EditorGUI.indentLevel--;

            EditorGUILayout.EndFoldoutHeaderGroup();
        }
    }
}