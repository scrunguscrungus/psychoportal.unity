﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace PsychoPortal.Unity
{
    public static class PS2
    {
        private static readonly Dictionary<string, string> _ps2Extensions = new()
        {
            [".plb"] = ".pl2",
            [".jan"] = ".ja2",
            [".tga"] = ".ps2",
            [".dds"] = ".ps2",
            [".tga"] = ".ps2",
            [".isb"] = ".pwb",
            [".icb"] = ".psb",
            [".bik"] = ".pss",
        };
        private static readonly Dictionary<string, string[]> _ps2LinkedExtensions = new()
        {
            [".ja2"] = new[] { ".pba", ".eve", ".txt", ".asd" },
            [".ps2"] = new[] { ".atx" },
        };

        private static string NormalizePath(string filePath) => filePath.Replace('\\', '/').ToLower();

        private static void AddFilePath(Loader[] loaders, HashSet<string> foundFiles, string filePath)
        {
            try
            {
                const string workResource = "workresource";
                int workResourceIndex = filePath.IndexOf(workResource, StringComparison.InvariantCultureIgnoreCase);

                if (workResourceIndex != -1)
                    filePath = filePath.Substring(workResourceIndex + workResource.Length + 1);

                string ext = Path.GetExtension(filePath).ToLowerInvariant();

                if (_ps2Extensions.ContainsKey(ext))
                {
                    ext = _ps2Extensions[ext];
                    filePath = Path.ChangeExtension(filePath, ext);
                }

                if (_ps2LinkedExtensions.ContainsKey(ext))
                    foreach (string linkedExt in _ps2LinkedExtensions[ext])
                        AddFilePath(loaders, foundFiles, Path.ChangeExtension(filePath, linkedExt));

                foreach (string path in GetPS2FilePaths(filePath))
                {
                    if (!loaders.Any(loader => loader.FileManager.FileExists(new FileRef(path))))
                        continue;

                    foundFiles.Add(NormalizePath(Path.Combine("workps2", path)));
                }
            }
            catch
            {
                // Ignore
            }
        }

        private static IEnumerable<string> GetPS2FilePaths(string filePath)
        {
            const string ps2Affix = "_ps2";

            string dir = Path.GetDirectoryName(filePath);
            string nameWithoutExt = Path.GetFileNameWithoutExtension(filePath);
            string ext = Path.GetExtension(filePath);

            if (nameWithoutExt.EndsWith(ps2Affix))
            {
                yield return filePath;
                yield return Path.Combine(dir, $"{nameWithoutExt.Substring(0, nameWithoutExt.Length - ps2Affix.Length)}{ext}");
            }
            else
            {
                yield return filePath;
                yield return Path.Combine(dir, $"{nameWithoutExt}{ps2Affix}{ext}");
            }
        }

        private static void AddFilesFromFileList(Loader[] loaders, HashSet<string> foundFiles, string dir, string listFile)
        {
            foreach (Loader loader in loaders)
            {
                using StreamReader wbTxt = new(loader.FileManager.OpenFile(new FileRef(listFile), throwIfNotFound: true));

                while (wbTxt.ReadLine() is { } line)
                    AddFilePath(loaders, foundFiles, $"{dir}/{line}");
            }
        }

        private static void AddFilesFromScript(Loader[] loaders, HashSet<string> foundFiles, string script)
        {
            for (int i = 0; i < script.Length - 4; i++)
            {
                if ((script[i + 0].ToString().Equals(".", StringComparison.InvariantCultureIgnoreCase) &&
                     script[i + 1].ToString().Equals("p", StringComparison.InvariantCultureIgnoreCase) &&
                     script[i + 2].ToString().Equals("l", StringComparison.InvariantCultureIgnoreCase) &&
                     script[i + 3].ToString().Equals("b", StringComparison.InvariantCultureIgnoreCase)) ||
                    (script[i + 0].ToString().Equals(".", StringComparison.InvariantCultureIgnoreCase) &&
                     script[i + 1].ToString().Equals("j", StringComparison.InvariantCultureIgnoreCase) &&
                     script[i + 2].ToString().Equals("a", StringComparison.InvariantCultureIgnoreCase) &&
                     script[i + 3].ToString().Equals("n", StringComparison.InvariantCultureIgnoreCase)))
                {
                    int startIndex = i;

                    while (startIndex >= 0 && script[startIndex] != '\'' && script[startIndex] != '\"')
                        startIndex--;

                    startIndex++;

                    string str = script.Substring(startIndex, i - startIndex + 4);
                    AddFilePath(loaders, foundFiles, str);
                    AddFilePath(loaders, foundFiles, str.Replace(@"\\", @"\").Replace(@"//", @"/"));
                }
            }
        }

        private static void AddFilesFromScene(Loader[] loaders, HashSet<string> foundFiles, Scene scene)
        {
            checkDomain(scene.RootDomain);

            void checkDomain(Domain domain)
            {
                foreach (string sceneRef in domain.RuntimeReferences)
                    AddFilePath(loaders, foundFiles, Path.ChangeExtension(sceneRef, ".pl2"));

                foreach (DomainEntityInfo entity in domain.DomainEntityInfos)
                    if (entity.EditVars != null)
                        AddFilesFromScript(loaders, foundFiles, entity.EditVars);

                foreach (Domain child in domain.Children)
                    checkDomain(child);
            }
        }

        // TODO: Include this in the loader?
        private static readonly string[] _ppfNames = 
        {
            "common",
            "STMU",
            "CARE",
            "CARE_NIGHT",
            "CAMA",
            "CAMA_NIGHT",
            "CAKC",
            "CAKC_NIGHT",
            "CABH",
            "CABH_NIGHT",
            "CALI",
            "CALI_NIGHT",
            "CAGP",
            "CAGP_NIGHT",
            "CAJA",
            "CASA",
            "CABU",
            "BBA1",
            "BBA2",
            "BBLT",
            "NIMP",
            "NIBA",
            "SACU",
            "MIFL",
            "MIMM",
            "MILL",
            "LLLL",
            "LOMA",
            "LOCB",
            "ASGR",
            "ASCO",
            "ASUP",
            "ASLB",
            "ASRU",
            "MMI1",
            "MMI2",
            "MMDM",
            "THMS",
            "THCW",
            "THFB",
            "WWMA",
            "BVES",
            "BVRB",
            "BVWT",
            "BVWE",
            "BVWD",
            "BVWC",
            "BVMA",
            "MCTC",
            "MCBB",
        };

        public static string GenerateFileTable(Loader[] ps2Loaders, Loader[] loaders, string sourceDirPath, IBinarySerializerLogger logger)
        {
            string[] hardCodedPaths =
            {
                "Sounds/PS2 Wavebanks/wavebanks.txt",
                "Sounds/PS2 Soundbanks/soundbanks.txt",
                "cutScenes/prerendered_pal/budcat.pss",
                "cutScenes/prerendered_pal/thq.pss",
                "cutScenes/prerendered/budcat.pss",
                "cutScenes/prerendered/thq.pss",
                "cutscenes/prerendered/gameplay.pss", // US demo
                "cutscenes/prerendered/gameplay.pss", // US demo
                "levels/actionland.pl2",
                "levels/actionland_night.pl2",
                "levels/dete.pl2",
                "levels/ASLC.pl2",
                "levels/BBA1LSOs.pl2",
                "levels/BBA2LSOs.pl2",
                "levels/BBLTLSOs.pl2",
                "levels/BVWA.pl2",
                "levels/CMBT.pl2",
                "levels/LOAR.pl2",
                "levels/MMtest.pl2",
                "levels/TEST_CubeGravity.pl2",
                "levels/Test_BeveledCube.pl2",
                "levels/ni_nightmares/nila.pl2",
            };

            foreach (Loader loader in ps2Loaders)
                loader.LoadFilePackages(logger);

            var foundFiles = new HashSet<string>();

            foreach (string path in hardCodedPaths)
                AddFilePath(ps2Loaders, foundFiles, path);

            // Sound files are listed in .txt files
            AddFilesFromFileList(ps2Loaders, foundFiles, "Sounds/PS2 Wavebanks", "Sounds/PS2 Wavebanks/wavebanks.txt");
            AddFilesFromFileList(ps2Loaders, foundFiles, "Sounds/PS2 Soundbanks", "Sounds/PS2 Soundbanks/soundbanks.txt");

            // Add primary unpacked level scenes
            foreach (string lvl in _ppfNames)
                AddFilePath(ps2Loaders, foundFiles, $"levels/{lvl}.pl2");

            // The PS2 version has raw duplicates of the textures and mesh files
            foreach (string ppfName in _ppfNames)
            {
                foreach (Loader loader in loaders.Concat(ps2Loaders))
                {
                    string ppfPath = loader.GetPackPackFilePath(ppfName);
                    using Stream stream = loader.FileManager.OpenFile(new FileRef(ppfPath, FileLocation.FileSystem), throwIfNotFound: false);

                    if (stream == null)
                        continue;

                    PackPack ppf = Binary.ReadFromStream<PackPack>(stream, loader.Settings, logger: logger,
                        onPreSerializing: (_, x) => x.Pre_IsCommon = ppfPath == loader.GetCommonPackPackFilePath(), name: ppfPath);

                    foreach (GameTexture tex in ppf.TexturePack.Textures)
                        AddFilePath(ps2Loaders, foundFiles, Path.ChangeExtension(tex.FileName, ".ps2"));

                    if (ppf.TexturePack.LocalizedTextures != null)
                        foreach (GameTexture tex in ppf.TexturePack.LocalizedTextures.SelectMany(x => x.Textures)) 
                            AddFilePath(ps2Loaders, foundFiles, Path.ChangeExtension(tex.FileName, ".ps2"));

                    foreach (PackedScene meshFile in ppf.MeshPack.MeshFiles)
                    {
                        AddFilePath(ps2Loaders, foundFiles, Path.ChangeExtension(meshFile.FileName, ".pl2"));

                        AddFilesFromScene(ps2Loaders, foundFiles, meshFile.Scene);
                    }

                    if (ppf.Scene != null)
                    {
                        AddFilesFromScene(ps2Loaders, foundFiles, ppf.Scene);

                        foreach (Scene scene in ppf.ReferencedScenes)
                            AddFilesFromScene(ps2Loaders, foundFiles, scene);
                    }
                }
            }

            if (Directory.Exists(sourceDirPath))
            {
                foreach (string file in Directory.EnumerateFiles(sourceDirPath, "*", SearchOption.AllDirectories))
                {
                    string relPath = file.Substring(sourceDirPath.Length + 1);

                    AddFilePath(ps2Loaders, foundFiles, relPath);
                    AddFilePath(ps2Loaders, foundFiles, Path.ChangeExtension(relPath, ".ps2"));

                    // Hacky code to check the lua files
                    if (file.EndsWith(".lua"))
                    {
                        string luaFile = File.ReadAllText(file);
                        AddFilesFromScript(ps2Loaders, foundFiles, luaFile);
                    }
                }
            }

            foreach (Loader loader in loaders)
            {
                loader.LoadFilePackages(logger);

                foreach (FileRef file in loader.FileManager.EnumerateFiles(FileLocation.Any))
                {
                    string ps2Path = file.GetNormalizedFilePathRoot();

                    ps2Path = ps2Path.Replace("PCLevelPackFiles", "LevelPackFiles");

                    string ext = Path.GetExtension(ps2Path);

                    // The PAL version changes "prerendered" to "prerendered_pal"
                    if (ext.Equals(".bik", StringComparison.InvariantCultureIgnoreCase))
                    {
                        int pi = ps2Path.IndexOf("prerendered", StringComparison.InvariantCultureIgnoreCase);
                        
                        if (pi != -1)
                            AddFilePath(ps2Loaders, foundFiles, ps2Path.Replace(ps2Path.Substring(pi, "prerendered".Length), "prerendered_pal"));
                    }

                    // Some localization files have a PS2 equivalent
                    if (ext.Equals(".lub", StringComparison.InvariantCultureIgnoreCase))
                        AddFilePath(ps2Loaders, foundFiles, $"{ps2Path.Substring(0, ps2Path.Length - 4)}PS2.lub");

                    AddFilePath(ps2Loaders, foundFiles, ps2Path);
                }
            }

            foreach (Loader ps2Loader in ps2Loaders)
            {
                foreach (PS2_FileEntry fileEntry in ps2Loader.FileManager.PS2_FileTable.Files)
                {
                    try
                    {
                        Scene scene = ps2Loader.FileManager.ReadFromFile<Scene>($"0x{fileEntry.FilePathHash:X8}");
                        AddFilesFromScene(ps2Loaders, foundFiles, scene);
                    }
                    catch
                    {
                        // Ignore
                    }
                }
            }

            var str = new StringBuilder();

            foreach (string file in foundFiles.OrderBy(x => x))
                str.AppendLine($"\t\t\t\"{file}\", // {PS2_FileEntry.GetFilePathHash(file):X8}");

            Debug.Log($"Found {foundFiles.Count} files");

            return str.ToString();
        }
    }
}