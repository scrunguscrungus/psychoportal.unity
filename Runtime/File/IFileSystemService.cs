﻿using System.Collections.Generic;
using System.IO;

namespace PsychoPortal.Unity
{
    public interface IFileSystemService
    {
        bool FileExists(string filePath);
        bool DirectoryExists(string dirPath);
        
        IEnumerable<string> EnumerateFiles(string dirPath, string searchPattern, SearchOption searchOption);
        
        Stream OpenFile(string filePath, FileAccess access);
    }
}