﻿using System;
using System.Linq;
using UnityEngine;

namespace PsychoPortal.Unity
{
    public class MeshAnimationComponent : MonoBehaviour
    {
        private bool _isInitialized;
        private int _animLength;

        public Mesh Mesh;
        public AnimAffector[] AnimAffectors => Mesh.AnimAffectors;

        public float AnimFrame;
        public bool Animate = true;
        public float AnimSpeed = 1;
        public float FrameRate = 60;

        private void Update()
        {
            if (!_isInitialized)
            {
                _isInitialized = true;
                InitAnimation();
            }

            // Update the animation frame
            if (Animate)
                AnimFrame = (AnimFrame + Time.deltaTime * FrameRate * AnimSpeed) % _animLength;

            foreach (AnimAffector affector in AnimAffectors)
                AnimateMesh(affector);
        }

        private void InitAnimation()
        {
            ResetAnimation();
            _animLength = AnimAffectors.Max(x => x.TotalFramesCount);
        }

        private void ResetAnimation()
        {
            AnimFrame = 0;
            transform.localPosition = Mesh.Position.ToVector3();
            transform.localRotation = Mesh.Rotation.ToQuaternionRad();
            transform.localScale = Mesh.Scale.ToVector3();
        }

        private void AnimateMesh(AnimAffector affector)
        {
            int keyFrameIndex;

            // Find the current key frame index we're at
            for (keyFrameIndex = 0; keyFrameIndex < affector.KeyFrames.Length; keyFrameIndex++)
            {
                if (affector.KeyFrames[keyFrameIndex].Time >= AnimFrame)
                    break;
            }

            // If we've exceeded the keyframes we always want to use the last frame
            if (keyFrameIndex == affector.KeyFrames.Length)
                keyFrameIndex = affector.KeyFrames.Length - 1;

            // Get the two key frames to blend
            AnimAffector_KeyFrame fromFrame = affector.KeyFrames[Math.Max(keyFrameIndex - 1, 0)];
            AnimAffector_KeyFrame toFrame = affector.KeyFrames[keyFrameIndex];

            // Calculate the ratio to blend by
            int framesDifference = toFrame.Time - fromFrame.Time;
            float lerpRatio = framesDifference == 0 ? 1 : (AnimFrame - fromFrame.Time) / framesDifference;

            // TODO: Optimize this by caching the converted value types (to avoid doing things like ToVector3() each frame)
            switch (affector.Type)
            {
                case AnimAffector.AnimAffectorType.Position:
                    transform.localPosition = Vector3.Lerp(
                        fromFrame.Vector.ToVector3(),
                        toFrame.Vector.ToVector3(),
                        lerpRatio);
                    break;

                case AnimAffector.AnimAffectorType.Rotation:
                    transform.localRotation = Quaternion.Slerp(
                        fromFrame.Vector.ToQuaternionRad(),
                        toFrame.Vector.ToQuaternionRad(),
                        lerpRatio);
                    break;

                case AnimAffector.AnimAffectorType.Scale:
                    transform.localScale = Vector3.Lerp(
                        fromFrame.Vector.ToVector3(),
                        toFrame.Vector.ToVector3(),
                        lerpRatio);
                    break;
            }
        }
    }
}