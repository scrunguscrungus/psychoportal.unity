﻿using System.Linq;
using UnityEngine;

namespace PsychoPortal.Unity
{
    public class MeshFragBlendComponent : MonoBehaviour
    {
        private float[] _prevWeights;

        public float[] BlendWeights;
        public UnityEngine.Mesh UnityMesh;
        public MeshFrag MeshFrag;

        public BlendshapeStream[] BlendshapeStreams => MeshFrag.BlendshapeData.Streams;

        public void Reset()
        {
            for (var i = 0; i < BlendWeights.Length; i++)
                BlendWeights[i] = 0;
        }

        private void Update()
        {
            BlendWeights ??= new float[BlendshapeStreams.Length];

            if (_prevWeights == null || _prevWeights.Length != BlendWeights.Length)
                _prevWeights = new float[BlendWeights.Length];

            bool modified = false;

            for (int i = 0; i < BlendWeights.Length; i++)
            {
                if (BlendWeights[i] == _prevWeights[i])
                    continue;

                modified = true;
                _prevWeights[i] = BlendWeights[i];
            }

            if (!modified)
                return;

            // TODO: Update normals as well (they don't use the scale)

            Vector3[] vertices = MeshFrag.Vertices.Select(x => x.Vertex.ToVector3()).ToArray();

            for (int i = 0; i < BlendWeights.Length; i++)
            {
                BlendshapeStream blendShape = BlendshapeStreams[i];

                foreach (VertexStreamSWBlend v in blendShape.Vertices)
                {
                    Vector3 deltaVert = v.Vertex.ToVector3() * blendShape.Scale;
                    vertices[v.Index] += deltaVert * BlendWeights[i];
                }
            }

            UnityMesh.SetVertices(vertices);
        }
    }
}