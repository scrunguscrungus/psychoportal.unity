﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PsychoPortal.Unity
{
    public class PsychonautsSkelAnim
    {
        public PsychonautsSkelAnim(FileRef fileRef)
        {
            FileRef = fileRef;
        }

        public PsychonautsSkelAnim(string filePath) : this(new FileRef(filePath, FileLocation.Package)) { }

        private FileRef FileRef { get; set; }
        public string FilePath => FileRef.FilePath;
        public LoadedAnimData LoadedData { get; private set; }

        // Header
        public SharedSkelAnimHeader Header { get; private set; }
        public string RootJointName { get; private set; }
        public int JointsCount => Header.Version >= 200 ? Header.JointsCount : JointAnimations?.Length ?? -1;
        public StubSharedSkelAnim StubAnim { get; private set; }

        // Components
        public EventAnim EventAnimation { get; private set; }
        public SharedBlendAnim BlendAnimation { get; private set; }

        // Full
        public SharedSkelAnim SharedAnim { get; private set; }
        public SharedJointAnim[] JointAnimations { get; private set; }

        public void InitHeader(StubSharedSkelAnim stub)
        {
            if (LoadedData.HasFlag(LoadedAnimData.Header))
                return;

            StubAnim = stub;
            FileRef = new FileRef(FilePath, StubAnim.JANFileDataOffset, StubAnim.JANFileSize);
            RootJointName = stub.RootJointName;
            Header = stub.Header;
            LoadedData |= LoadedAnimData.Header;
        }

        public void InitHeader(FileManager fileManager, IBinarySerializerLogger logger = null)
        {
            if (LoadedData.HasFlag(LoadedAnimData.Header))
                return;

            var stub = fileManager.ReadFromFile<StubPackagedSharedSkelAnim>(FileRef, logger, throwIfNotFound: true, logIfNotFullyRead: false);

            RootJointName = stub.RootJointName;
            Header = stub.Header;
            LoadedData |= LoadedAnimData.Header;
        }

        public void InitComponents(FileManager fileManager, IBinarySerializerLogger logger = null)
        {
            if (LoadedData.HasFlag(LoadedAnimData.Components))
                return;

            // Temporary as most PS2 animation component files are empty or have garbage data in the PAL version
            if (fileManager.Version == PsychonautsVersion.PS2)
            {
                LoadedData |= LoadedAnimData.Components;
                return;
            }

            if (StubAnim != null)
            {
                // Read the event animation
                if (StubAnim.EventAnim != null)
                    EventAnimation = Binary.ReadFromBuffer<EventAnim>(StubAnim.EventAnim, fileManager.Settings, logger: logger);

                // Read the blend animation
                if (StubAnim.BlendAnim != null)
                    BlendAnimation = Binary.ReadFromBuffer<SharedBlendAnim>(StubAnim.BlendAnim, fileManager.Settings, logger: logger);
            }
            else
            {
                // Read the event animation
                EventAnimation = fileManager.ReadFromFile<EventAnim>(new FileRef(Path.ChangeExtension(FilePath, ".eve"), FileLocation.Package), logger, throwIfNotFound: false);

                // Read the event animation
                BlendAnimation = fileManager.ReadFromFile<SharedBlendAnim>(new FileRef(Path.ChangeExtension(FilePath, ".pba"), FileLocation.Package), logger, throwIfNotFound: false);
            }

            LoadedData |= LoadedAnimData.Components;
        }

        public void Init(FileManager fileManager, IBinarySerializerLogger logger = null)
        {
            if (LoadedData.HasFlag(LoadedAnimData.Full))
                return;

            SharedAnim = fileManager.ReadFromFile<SharedSkelAnim>(FileRef, logger, throwIfNotFound: true);
            RootJointName = SharedAnim.RootJoint.Name;
            Header = SharedAnim.Header;

            var jointAnims = new List<SharedJointAnim>();

            addJointAnim(SharedAnim.RootJoint);

            JointAnimations = jointAnims.ToArray();

            void addJointAnim(SharedJointAnim j)
            {
                jointAnims.Add(j);

                foreach (SharedJointAnim child in j.Children)
                    addJointAnim(child);
            }

            // Read the components
            InitComponents(fileManager, logger);

            LoadedData |= LoadedAnimData.Full;
        }

        [Flags]
        public enum LoadedAnimData
        {
            None = 0,
            Header = 1 << 0,
            Components = 1 << 1,
            Full = 1 << 2 | Components | Header,
        }

        // When we initialize it as stub from a package we want to avoid reading the entire joint animation data since that's slow, so just
        // read the header and the name of the root joint
        private class StubPackagedSharedSkelAnim : IBinarySerializable
        {
            public SharedSkelAnimHeader Header { get; set; }
            public String4b RootJointName { get; set; }

            public void Serialize(IBinarySerializer s)
            {
                Header = s.SerializeObject<SharedSkelAnimHeader>(Header, name: nameof(Header));
                RootJointName = s.SerializeObject<String4b>(RootJointName, name: nameof(RootJointName));
            }
        }
    }
}