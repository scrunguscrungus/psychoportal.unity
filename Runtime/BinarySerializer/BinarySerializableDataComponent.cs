﻿using UnityEngine;

namespace PsychoPortal.Unity
{
    /// <summary>
    /// A component for allowing <see cref="IBinarySerializable"/> data to be viewed and edited
    /// </summary>
    public class BinarySerializableDataComponent : MonoBehaviour
    {
        public IBinarySerializable Data { get; set; }
        public PsychonautsSettings Settings { get; set; }
    }
}