﻿using System.Linq;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace PsychoPortal.Unity
{
    public class FilePathDropDown : AdvancedDropdown
    {
        public FilePathDropDown(AdvancedDropdownState state, string name, string[] filePaths, string selectedFilePath = "") : base(state)
        {
            Name = name;
            FilePaths = filePaths;
            SelectedFilePath = selectedFilePath;

            minimumSize = new Vector2(70, 400);
        }

        private readonly char[] _pathSeparators = { '/', '\\' };

        public string Name { get; }
        public string[] FilePaths { get; }
        public string SelectedFilePath { get; set; }
        public bool IsDirty { get; set; }

        protected override AdvancedDropdownItem BuildRoot()
        {
            var root = new AdvancedDropdownItem(Name);

            for (int i = 0; i < FilePaths.Length; i++)
            {
                Add(root, FilePaths[i], FilePaths[i], i);
            }

            return root;
        }

        protected void Add(AdvancedDropdownItem parent, string path, string fullPath, int id)
        {
            if (path.Contains('/') || path.Contains('\\'))
            {
                string folder = path.Substring(0, path.IndexOfAny(_pathSeparators));
                string rest = path.Substring(path.IndexOfAny(_pathSeparators) + 1);
                
                AdvancedDropdownItem dirNode = parent.children.FirstOrDefault(c => c.name == folder);
                
                if (dirNode == null)
                {
                    dirNode = new AdvancedDropdownItem(folder);
                    parent.AddChild(dirNode);
                }
                
                Add(dirNode, rest, fullPath, id);
            }
            else
            {
                parent.AddChild(new AdvancedDropdownItem(path)
                {
                    id = id
                });
            }
        }

        protected override void ItemSelected(AdvancedDropdownItem item)
        {
            if (!item.children.Any() && FilePaths != null && FilePaths.Length > item.id)
            {
                SelectedFilePath = FilePaths[item.id];
                IsDirty = true;
            }
        }
    }
}