﻿using System;

namespace PsychoPortal.Unity
{
    public readonly struct FileRef
    {
        public FileRef(string filePath)
        {
            FilePath = filePath;
            Location = FileLocation.Any;
            FileOffset = 0xFFFFFFFF;
            FileSize = 0xFFFFFFFF;
        }

        public FileRef(string filePath, FileLocation location)
        {
            FilePath = filePath;
            Location = location;
            FileOffset = 0xFFFFFFFF;
            FileSize = 0xFFFFFFFF;
        }

        public FileRef(string filePath, uint fileOffset, uint fileSize)
        {
            FilePath = filePath;
            Location = FileLocation.Package;
            FileOffset = fileOffset;
            FileSize = fileSize;
        }

        public static implicit operator FileRef(string path) => new FileRef(path); 

        private const string _workResource = "WorkResource";

        public string FilePath { get; }
        public FileLocation Location { get; }
        public uint FileOffset { get; }
        public uint FileSize { get; }

        public bool HasFileOffset => FileOffset != 0xFFFFFFFF;
        public bool HasFileSize => FileSize != 0xFFFFFFFF;

        public string GetNormalizedFilePathRoot()
        {
            string normalizedPath = FilePath;

            int rootCharIndex = normalizedPath.IndexOf(_workResource, StringComparison.InvariantCultureIgnoreCase);

            if (rootCharIndex != -1 &&
                (normalizedPath[rootCharIndex + _workResource.Length] == '/' || normalizedPath[rootCharIndex + _workResource.Length] == '\\'))
                normalizedPath = normalizedPath.Substring(rootCharIndex + _workResource.Length + 1);

            return normalizedPath;
        }
    }
}