﻿using System;
using UnityEngine;

namespace PsychoPortal.Unity
{
    /// <summary>
    /// Helper methods for meshes
    /// </summary>
    public static class MeshHelpers
    {
        public static int[] TriStripToTriangles(short[] triStripPoints, bool flip = false)
        {
            if (triStripPoints.Length < 3)
            {
                Debug.LogWarning("Provided triangle strip points are fewer than 3 and can thus not be converted");
                return Array.Empty<int>();
            }

            int[] triTable = new int[(triStripPoints.Length - 2) * 3];

            int triTableIndex = 0;

            for (int i = 2; i < triStripPoints.Length; i++)
            {
                // Ensure all triangles are clockwise by alternating
                if (flip)
                {
                    triTable[triTableIndex] = triStripPoints[i - 1];
                    triTableIndex++;

                    triTable[triTableIndex] = triStripPoints[i - 2];
                    triTableIndex++;
                }
                else
                {
                    triTable[triTableIndex] = triStripPoints[i - 2];
                    triTableIndex++;

                    triTable[triTableIndex] = triStripPoints[i - 1];
                    triTableIndex++;
                }

                flip = !flip;

                triTable[triTableIndex] = triStripPoints[i];
                triTableIndex++;
            }

            return triTable;
        }

        public static void FlipTriIndices(int[] indices, int inIndex, int outIndex, int length)
        {
            for (int i = 0; i < length; i += 3)
            {
                indices[outIndex + i + 0] = indices[inIndex + i + 0];
                (indices[outIndex + i + 1], indices[outIndex + i + 2]) = (indices[inIndex + i + 2], indices[inIndex + i + 1]);
            }
        }
    }
}