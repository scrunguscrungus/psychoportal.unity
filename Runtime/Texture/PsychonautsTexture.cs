﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace PsychoPortal.Unity
{
    public class PsychonautsTexture
    {
        /// <summary>
        /// Creates a new Psychonauts texture
        /// </summary>
        /// <param name="gameTexture">The serialized game texture</param>
        /// <param name="nativeTexture">If true the created Unity <see cref="Texture2D"/> objects will use their native format
        /// equivalents rather than be converted to standard RGB textures. Settings this to true will slightly improve performance, but
        /// won't allow compressed textures to be exported and may not be supported on all systems.</param>
        public PsychonautsTexture(GameTexture gameTexture, bool nativeTexture)
        {
            _nativeTexture = nativeTexture;
            GameTexture = gameTexture;
        }

        private readonly bool _nativeTexture;
        private Texture2D[] _textures_clamp;
        private Texture2D[] _textures_repeat;

        public GameTexture GameTexture { get; }
        public TexAnimInfo AnimInfo => GameTexture.AnimInfo;
        public bool IsAnimated => AnimInfo != null;

        public Texture2D GetTexture(bool repeat) => GetTextureFrames(repeat)[0];

        public Texture2D[] GetTextureFrames(bool repeat)
        {
            if (repeat)
                return _textures_repeat ??= GameTexture.Frames.Select(x => ToTexture2D(x, true)).ToArray();
            else
                return _textures_clamp ??= GameTexture.Frames.Select(x => ToTexture2D(x, false)).ToArray();
        }

        private Texture2D ToTexture2D(TextureFrame frame, bool repeat)
        {
            try
            {
                // Handle PS2 textures separately
                if (frame.PS2_Faces != null)
                    return frame.PS2_Faces[0].ToTexture2D();

                switch (frame.Type)
                {
                    case TextureType.Bitmap:
                        return ToTexture2D(frame.Faces[0], frame.Format, (int)frame.Width, (int)frame.Height, repeat);

                    case TextureType.Cubemap: // TODO: Use Cubemap type? Need to return Texture instead of Texture2D then.
                    case TextureType.VolumeMap:
                    case TextureType.DepthBuffer:
                    default:
                        Debug.LogWarning($"Not implemented texture type: {frame.Type}");
                        return null;
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"Failed to load texture {GameTexture.FileName}. Error: {ex.Message}{Environment.NewLine}{ex}");
                return null;
            }
        }

        private static GraphicsFormat GetUnityGraphicsFormat(TextureFormat format) => format switch
        {
            TextureFormat.Format_8888 => GraphicsFormat.R8G8B8A8_UNorm,
            TextureFormat.Format_0888 => GraphicsFormat.R8G8B8_UNorm,
            TextureFormat.Format_DXT1 => GraphicsFormat.RGBA_DXT1_UNorm,
            TextureFormat.Format_DXT3 => GraphicsFormat.RGBA_DXT3_UNorm,
            TextureFormat.Format_DXT5 => GraphicsFormat.RGBA_DXT5_UNorm,
            TextureFormat.Format_4444 => GraphicsFormat.R4G4B4A4_UNormPack16,
            TextureFormat.Format_1555 => GraphicsFormat.A1R5G5B5_UNormPack16,
            TextureFormat.Format_0555 => GraphicsFormat.R5G6B5_UNormPack16,
            TextureFormat.Format_565 => GraphicsFormat.R5G6B5_UNormPack16,
            // TODO: Some of the formats not in GraphicsFormat seem toe exist in UnityEngine.TextureFormat - use those then?
            _ => GraphicsFormat.None
        };

        private Texture2D ToTexture2D(TextureFace face, TextureFormat format, int width, int height, bool repeat)
        {
            // Get the Unity graphics format
            GraphicsFormat unityFormat = _nativeTexture ? GetUnityGraphicsFormat(format) : GraphicsFormat.None;
            TextureWrapMode wrapMode = repeat ? TextureWrapMode.Repeat : TextureWrapMode.Clamp;

            // Make sure the format is supported
            if (unityFormat != GraphicsFormat.None && !SystemInfo.IsFormatSupported(unityFormat, FormatUsage.Sample))
            {
                Debug.LogWarning($"Format {format} is not supported on system");
                unityFormat = GraphicsFormat.None;
            }
            
            // If the format is not supported, fall back to manually setting it
            if (unityFormat == GraphicsFormat.None)
            {
                Texture2D tex = new Texture2D(width, height, UnityEngine.TextureFormat.RGBA32, false)
                {
                    wrapMode = wrapMode
                };
                
                tex.SetPixels(TextureHelpers.GetPixelColors(format, face.Surfaces[0].ImageData, width, height));
                tex.Apply();

                return tex;
            }
            // If supported we load the raw data
            else
            {
                Texture2D tex = new Texture2D(width, height, unityFormat, TextureCreationFlags.None)
                {
                    wrapMode = wrapMode
                };

                tex.LoadRawTextureData(face.Surfaces[0].ImageData);
                tex.Apply();

                return tex;
            }
        }
    }
}