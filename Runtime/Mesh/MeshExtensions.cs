﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityMesh = UnityEngine.Mesh;

namespace PsychoPortal.Unity
{
    /// <summary>
    /// Extension methods for <see cref="UnityEngine.Mesh"/>
    /// </summary>
    public static class MeshExtensions
    {
        public static void SetVertices(this UnityMesh mesh, MeshFrag frag) =>
            mesh.SetVertices(frag.Vertices.Select(x => x.Vertex.ToVector3()).ToArray());

        public static void SetNormals(this UnityMesh mesh, MeshFrag frag) =>
            mesh.SetNormals(frag.Vertices.Select(x => x.Normal.ToVector3()).ToArray());

        public static void SetPolygons(this UnityMesh mesh, MeshFrag frag, int subMesh = 0)
        {
            // Get the material flags
            MaterialFlags matFlags = frag.MaterialFlags;

            // Set polygon indices. Unity doesn't support triangle strips anymore, so we need to convert it.
            int[] indices = matFlags.HasFlag(MaterialFlags.Tristrip)
                ? MeshHelpers.TriStripToTriangles(frag.PolygonIndexBuffer, flip: true)
                : frag.PolygonIndexBuffer.Select(x => (int)x).ToArray();

            // Duplicate triangles if double sided
            if (matFlags.HasFlag(MaterialFlags.DoubleSided))
            {
                int origLength = indices.Length;
                Array.Resize(ref indices, indices.Length * 2);

                MeshHelpers.FlipTriIndices(indices, 0, origLength, origLength);
            }
            else if (!frag.MaterialFlags.HasFlag(MaterialFlags.Tristrip))
            {
                MeshHelpers.FlipTriIndices(indices, 0, 0, indices.Length);
            }

            mesh.SetTriangles(indices, subMesh);
        }

        public static void SetVertexColors(this UnityMesh mesh, MeshFrag frag, float factor = 2f)
        {
            IEnumerable<BGRA8888Color> colors = frag.GetVertexColors();

            if (colors != null)
                mesh.SetColors(colors.Select(x => x.ToColor(factor)).ToArray());
        }

        public static void SetUVs(this UnityMesh mesh, MeshFrag frag, int unityUVChannel, int psychonautsUVChannel) =>
            mesh.SetUVs(unityUVChannel, frag.UVSets.Select(x => x.UVs[psychonautsUVChannel].ToVec2(frag.UVScale).ToVector2()).ToArray());
    }
}