﻿using System.Collections.Generic;
using System.IO;

namespace PsychoPortal.Unity
{
    public class FileSystemService : IFileSystemService
    {
        public virtual bool FileExists(string filePath) => File.Exists(filePath);
        public virtual bool DirectoryExists(string dirPath) => Directory.Exists(dirPath);
        
        public virtual IEnumerable<string> EnumerateFiles(string dirPath, string searchPattern, SearchOption searchOption) => 
            Directory.EnumerateFiles(dirPath, searchPattern, searchOption);
        
        public virtual Stream OpenFile(string filePath, FileAccess access)
        {
            FileShare share = access == FileAccess.Read ? FileShare.Read : FileShare.None;
            FileMode mode = access == FileAccess.Read ? FileMode.Open : FileMode.OpenOrCreate;

            return new FileStream(filePath, mode, access, share);
        }
    }
}