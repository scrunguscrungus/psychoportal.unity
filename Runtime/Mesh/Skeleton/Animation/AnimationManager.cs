﻿using System.Collections.Generic;
using System.Linq;

namespace PsychoPortal.Unity
{
    public class AnimationManager
    {
        private readonly Dictionary<string, PsychonautsSkelAnim> _anims = new Dictionary<string, PsychonautsSkelAnim>();

        public IEnumerable<string> AnimationFilePaths => _anims.Keys;
        public IEnumerable<PsychonautsSkelAnim> Animations => _anims.Values;
        public int AnimationsCount => _anims.Count;

        private static string NormalizeFilePath(string filePath) => filePath.Replace('/', '\\').ToLower();

        public void LoadStubAnimations(AnimPack animPack)
        {
            if (animPack == null)
                return;

            LoadStubAnimations(animPack.StubSharedAnims);
        }

        public void LoadStubAnimations(IEnumerable<StubSharedSkelAnim> stubAnims)
        {
            foreach (StubSharedSkelAnim stub in stubAnims)
            {
                var anim = new PsychonautsSkelAnim(stub.JANFileName);
                anim.InitHeader(stub);
                _anims.Add(NormalizeFilePath(stub.JANFileName), anim);
            };
        }

        public void LoadAnimation(FileRef fileRef, FileManager fileManager, IBinarySerializerLogger logger = null)
        {
            var anim = new PsychonautsSkelAnim(fileRef);
            anim.InitHeader(fileManager, logger);
            _anims.Add(NormalizeFilePath(anim.FilePath), anim);
        }

        public bool HasLoadedAnimation(string filePath) => _anims.ContainsKey(NormalizeFilePath(filePath));

        public PsychonautsSkelAnim GetAnimation(string filePath) => _anims.TryGetValue(NormalizeFilePath(filePath), out PsychonautsSkelAnim anim) ? anim : null;

        public IEnumerable<PsychonautsSkelAnim> GetValidAnimations(PsychonautsMesh mesh) => Animations.Where(mesh.CanPlayAnimation);
    }
}