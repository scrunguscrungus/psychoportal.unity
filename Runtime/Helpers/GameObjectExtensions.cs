﻿using System;
using UnityEngine;

namespace PsychoPortal.Unity
{
    public static class GameObjectExtensions
    {
        public static T AddComponent<T>(this GameObject obj, Action<T> configureAction)
            where T : Component
        {
            T comp = obj.AddComponent<T>();
            configureAction(comp);
            return comp;
        }

        public static void AddBinarySerializableData(this GameObject obj, PsychonautsSettings settings, IBinarySerializable data)
        {
            obj.AddComponent<BinarySerializableDataComponent>(x =>
            {
                x.Settings = settings;
                x.Data = data;
            });
        }
    }
}