﻿namespace PsychoPortal.Unity
{
    public class InspectorBitSerializer : IBitSerializer
    {
        public InspectorBitSerializer(InspectorSerializer serializer)
        {
            Serializer = serializer;
        }

        public InspectorSerializer Serializer { get; }
        IBinarySerializer IBitSerializer.Serializer => Serializer;
        public long Value { get; set; }
        public int Position { get; set; }

        public T SerializeBits<T>(T value, int length, string name = null)
        {
            T t = Serializer.Serialize<T>(value, name);
            Position += length;
            return t;
        }
    }
}