﻿using UnityEngine;

namespace PsychoPortal.Unity
{
    public class PsychonautsJoint
    {
        public PsychonautsJoint(Joint joint, GameObject jointObj)
        {
            Joint = joint;
            Transform = jointObj.transform;
        }
        public PsychonautsJoint(Joint joint, Transform transform)
        {
            Joint = joint;
            Transform = transform;
        }

        public Joint Joint { get; }
        public Transform Transform { get; }

        public void ResetTransform()
        {
            Transform.localPosition = Joint.Position.ToVector3();

            if (Joint.Rotation.ToVector3() == new Vector3(-1, -1, -1))
                Transform.localRotation = Quaternion.identity; // TODO: Billboard!
            else
                Transform.localRotation = Joint.Rotation.ToQuaternionDeg();

            Transform.localScale = Vector3.one;
        }
    }
}