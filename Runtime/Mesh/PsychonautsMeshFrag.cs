﻿namespace PsychoPortal.Unity
{
    public class PsychonautsMeshFrag
    {
        public PsychonautsMeshFrag(MeshFrag meshFrag, UnityEngine.Mesh unityMesh, MeshFragBlendComponent blendComponent)
        {
            MeshFrag = meshFrag;
            UnityMesh = unityMesh;
            BlendComponent = blendComponent;
        }

        public MeshFrag MeshFrag { get; }
        public UnityEngine.Mesh UnityMesh { get; }
        public MeshFragBlendComponent BlendComponent { get; }
    }
}