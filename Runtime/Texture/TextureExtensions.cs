﻿using System.IO;
using UnityEngine;

namespace PsychoPortal.Unity
{
    /// <summary>
    /// Extension methods for <see cref="Texture"/>
    /// </summary>
    public static class TextureExtensions
    {
        public static void ExportPNG(this Texture tex, string filePath)
        {
            if (tex == null)
                return;

            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            if (tex is Texture2D tex2D)
                File.WriteAllBytes(filePath, tex2D.EncodeToPNG());
            else
                Debug.LogWarning($"Exporting texture of type {tex.GetType().Name} is currently not supported");
        }
    }
}