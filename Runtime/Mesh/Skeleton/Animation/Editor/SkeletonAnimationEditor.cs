﻿using System;
using System.Linq;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace PsychoPortal.Unity
{
    [CustomEditor(typeof(SkeletonAnimationComponent))]
    [CanEditMultipleObjects]
    public class SkeletonAnimationEditor : Editor
    {
        private FilePathDropDown _filePathDropDown;
        
        private PsychonautsSkelAnim _prevSkelAnim;
        private string _eventsOutput;

        public override void OnInspectorGUI()
        {
            SkeletonAnimationComponent comp = (SkeletonAnimationComponent)serializedObject.targetObject;

            if (EditorGUILayout.LinkButton("Clear"))
            {
                comp.AnimFile = "";
                _filePathDropDown.SelectedFilePath = "";
            }

            _filePathDropDown ??= new FilePathDropDown(new AdvancedDropdownState(), "Animation File", 
                comp.AnimationManager.GetValidAnimations(comp.Mesh).Select(x => x.FilePath).ToArray(), comp.AnimFile);

            UnityEngine.Rect rect = GUILayoutUtility.GetRect(new GUIContent(_filePathDropDown.SelectedFilePath), EditorStyles.toolbarButton);

            var dropButton = EditorGUI.PrefixLabel(rect, new GUIContent("Animation File"));
            rect = new UnityEngine.Rect(dropButton.x + dropButton.width - Mathf.Max(400f, dropButton.width), dropButton.y, Mathf.Max(400f, dropButton.width), dropButton.height);

            if (EditorGUI.DropdownButton(dropButton, new GUIContent(_filePathDropDown.SelectedFilePath), FocusType.Passive))
                _filePathDropDown.Show(rect);

            if (_filePathDropDown.IsDirty)
            {
                _filePathDropDown.IsDirty = false;
                comp.AnimFile = _filePathDropDown.SelectedFilePath;
            }

            //comp.AnimFile = EditorGUILayout.TextField("Animation File", comp.AnimFile);
            
            EditorGUILayout.Separator();
            
            comp.Animate = EditorGUILayout.Toggle("Animate", comp.Animate);
            comp.AnimFrame = Mathf.Clamp(EditorGUILayout.FloatField("Frame", comp.AnimFrame), 0, comp.AnimLength);
            
            EditorGUILayout.Separator();
            
            comp.AnimSpeed = EditorGUILayout.FloatField("AnimSpeed", comp.AnimSpeed);
            comp.FrameRate = EditorGUILayout.FloatField("FrameRate", comp.FrameRate);

            EditorGUILayout.Separator();

            if (_prevSkelAnim != comp.Anim)
            {
                _prevSkelAnim = comp.Anim;

                if (comp.Anim?.EventAnimation != null)
                {
                    var keyFrames = comp.Anim.EventAnimation.Channels.Select((x, i) => new
                    {
                        Keys = x.KeyFrames,
                        ChannelID = i,
                        Items = comp.Anim.EventAnimation.GetItems(i),
                    }).SelectMany(x => x.Keys.Select((k, i) => new
                    {
                        Time = k.Time,
                        Float_04 = k.Float_04,
                        ChannelID = x.ChannelID,
                        Item = x.Items[i % x.Items.Length],
                    })).OrderBy(x => x.Time);

                    _eventsOutput = String.Join(Environment.NewLine, keyFrames.Select(x =>
                        $"Frame: {x.Time:000}    |    " +
                        //$"Float_04: {x.Float_04}    |    " +
                        $"ChannelID: {x.ChannelID}    |    " +
                        $"Item: {x.Item}"));
                }
                else
                {
                    _eventsOutput = null;
                }
            }

            if (_eventsOutput != null)
            {
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.TextArea(_eventsOutput);
                EditorGUI.EndDisabledGroup();
            }
        }
    }
}