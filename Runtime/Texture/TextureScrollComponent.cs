﻿using UnityEngine;

namespace PsychoPortal.Unity
{
    public class TextureScrollComponent : MonoBehaviour
    {
        public Material material;
        public string textureName = "_MainTex";
        public Vector2 scroll;
        private Vector2 current;

        private void Update()
        {
            current = new Vector2(
                (current.x + Time.deltaTime * scroll.x) % 1f,
                (current.y + Time.deltaTime * scroll.y) % 1f);

            material.SetTextureOffset(textureName, current);
        }
    }
}