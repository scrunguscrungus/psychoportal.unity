﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace PsychoPortal.Unity
{
    /// <summary>
    /// Manages loading Psychonauts textures from texture packs
    /// </summary>
    public class TexturesManager
    {
        public TexturesManager(PsychonautsSettings settings, FileManager fileManager)
        {
            Settings = settings;
            FileManager = fileManager;

            RawTextures = new Dictionary<string, PsychonautsTexture>();
            PackedTextures = new Dictionary<TexturePack, Dictionary<string, PsychonautsTexture>>();
        }

        public PsychonautsSettings Settings { get; }
        public FileManager FileManager { get; }

        private Dictionary<string, PsychonautsTexture> RawTextures { get; }

        /// <summary>
        /// The loaded packed textures
        /// </summary>
        private Dictionary<TexturePack, Dictionary<string, PsychonautsTexture>> PackedTextures { get; }

        /// <summary>
        /// Loads the textures from a texture pack
        /// </summary>
        /// <param name="texturePack">The texture pack to load from</param>
        /// <param name="nativeTexture">If true the created Unity <see cref="Texture2D"/> objects will use their native format
        /// equivalents rather than be converted to standard RGB textures. Settings this to true will slightly improve performance, but
        /// won't allow compressed textures to be exported and may not be supported on all systems.</param>
        /// <param name="language">The language for localized textures</param>
        public void LoadPackedTextures(TexturePack texturePack, bool nativeTexture, GameLanguage language = GameLanguage.English)
        {
            PackedTextures[texturePack] = new Dictionary<string, PsychonautsTexture>();

            LocalizedTextures locTextures = texturePack.LocalizedTextures?.FirstOrDefault(x => x.Language == language);

            if (locTextures != null)
                foreach (GameTexture tex in locTextures.Textures)
                    PackedTextures[texturePack][tex.FileName] = new PsychonautsTexture(tex, nativeTexture);

            foreach (GameTexture tex in texturePack.Textures)
                PackedTextures[texturePack][tex.FileName] = new PsychonautsTexture(tex, nativeTexture);
        }

        /// <summary>
        /// Unloads the textures from a texture pack
        /// </summary>
        /// <param name="texturePack"></param>
        public void UnloadPackedTextures(TexturePack texturePack)
        {
            if (texturePack is null)
                return;

            PackedTextures.Remove(texturePack);
        }

        public void UnloadRawTextures() => RawTextures.Clear();

        /// <summary>
        /// Gets the textures from an array of references
        /// </summary>
        /// <param name="texRefs">The texture references</param>
        /// <param name="nativeTexture">If true the created Unity <see cref="Texture2D"/> objects will use their native format
        /// equivalents rather than be converted to standard RGB textures. Settings this to true will slightly improve performance, but
        /// won't allow compressed textures to be exported and may not be supported on all systems.</param>
        /// <param name="logger">An optional logger for if the texture has to be read</param>
        /// <returns>The textures. Textures which could not be found are null.</returns>
        public PsychonautsTexture[] GetTextures(TextureReference[] texRefs, bool nativeTexture, IBinarySerializerLogger logger = null) => 
            texRefs.Select(x => GetTexture(x, nativeTexture, logger)).ToArray();

        /// <summary>
        /// Gets the texture from a reference
        /// </summary>
        /// <param name="texRef">The texture reference</param>
        /// <param name="nativeTexture">If true the created Unity <see cref="Texture2D"/> objects will use their native format
        /// equivalents rather than be converted to standard RGB textures. Settings this to true will slightly improve performance, but
        /// won't allow compressed textures to be exported and may not be supported on all systems.</param>
        /// <param name="logger">An optional logger for if the texture has to be read</param>
        /// <returns>The texture or null if not found</returns>
        public PsychonautsTexture GetTexture(TextureReference texRef, bool nativeTexture, IBinarySerializerLogger logger = null) => 
            GetTexture(texRef.GetNormalizedTextureName(Settings.Version), nativeTexture, logger);

        /// <summary>
        /// Gets the texture from a normalized file path
        /// </summary>
        /// <param name="filePath">The normalized case-sensitive file path</param>
        /// <param name="nativeTexture">If true the created Unity <see cref="Texture2D"/> objects will use their native format
        /// equivalents rather than be converted to standard RGB textures. Settings this to true will slightly improve performance, but
        /// won't allow compressed textures to be exported and may not be supported on all systems.</param>
        /// <param name="logger">An optional logger for if the texture has to be read</param>
        /// <returns>The texture or null if not found</returns>
        public PsychonautsTexture GetTexture(string filePath, bool nativeTexture, IBinarySerializerLogger logger = null)
        {
            // Try to find a loaded texture
            PsychonautsTexture tex = PackedTextures.Values.Append(RawTextures).
                Select(x => x.TryGetValue(filePath, out PsychonautsTexture t) ? t : null).
                FirstOrDefault(x => x != null);

            // Return if we found a texture
            if (tex != null) 
                return tex;

            // TODO: When reading raw textures we need to check for animation files and parse them

            GameTexture gameTex;

            // Try loading the raw texture
            if (Settings.Version == PsychonautsVersion.PS2)
            {
                // Try reading the file
                PS2_Texture ps2Tex = FileManager.ReadFromFile<PS2_Texture>(filePath, logger: logger);

                if (ps2Tex == null)
                {
                    Debug.LogWarning($"Could not find texture {filePath}");
                    return null;
                }

                // Convert to a loaded texture
                gameTex = new GameTexture
                {
                    FileName = filePath,
                    AnimInfo = null,
                    Frames = new TextureFrame[]
                    {
                        new TextureFrame
                        {
                            Format = TextureFormat.Format_P8,
                            Type = TextureType.Bitmap, // TODO: How do we determine this? Does the PS2 version have cubemaps?
                            Width = ps2Tex.Width,
                            Height = ps2Tex.Height,
                            PS2_Faces = new PS2_Texture[] { ps2Tex }
                        }
                    }
                };
            }
            else
            {
                // Try reading the file
                DDS dds = FileManager.ReadFromFile<DDS>(filePath, logger: logger);

                if (dds == null)
                {
                    Debug.LogWarning($"Could not find texture {filePath}");
                    return null;
                }

                TextureFormat format = dds.Header.PixelFormat.GetTextureFormat();
                
                Debug.Log($"Loaded texture {filePath} from raw DDS file using format {format}");

                // Convert to a loaded texture
                gameTex = new GameTexture
                {
                    FileName = filePath,
                    AnimInfo = null,
                    Frames = new TextureFrame[]
                    {
                        new TextureFrame
                        {
                            Type = dds.IsCubeMap ? TextureType.Cubemap : TextureType.Bitmap,
                            Format = format,
                            Width = dds.Header.Width,
                            Height = dds.Header.Height,
                            MipMapLevels = dds.Header.GetMipMapCount,
                            HasPalette = (ushort)(format == TextureFormat.Format_P8 ? 1 : 0),
                            Palette = dds.Palette,
                            Faces = dds.Faces,
                        }
                    }
                };
            }

            var psychoTex = new PsychonautsTexture(gameTex, nativeTexture);

            RawTextures[filePath] = psychoTex;

            return psychoTex;

        }

        /// <summary>
        /// Dumps all the loaded packed textures to the specified directory
        /// </summary>
        /// <param name="outputDir">The directory to dump the textures to</param>
        public void DumpPackedTextures(string outputDir)
        {
            foreach (PsychonautsTexture tex in PackedTextures.Values.SelectMany(x => x.Values))
            {
                try
                {
                    if (tex.IsAnimated)
                    {
                        Texture2D[] frames = tex.GetTextureFrames(false);
                        
                        for (int i = 0; i < frames.Length; i++)
                            frames[i].ExportPNG(Path.Combine(outputDir, $"{tex.GameTexture.FileName}_{i}.png"));
                    }
                    else
                    {
                        tex.GetTexture(false).ExportPNG(Path.Combine(outputDir, $"{tex.GameTexture.FileName}.png"));
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError($"Failed to export texture {tex.GameTexture.FileName}: {ex.Message}{Environment.NewLine}{ex}");
                }
            }
        }
    }
}