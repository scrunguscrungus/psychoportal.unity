﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace PsychoPortal.Unity
{
    public class InspectorSerializer : IBinarySerializer
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="settings">The settings to use when serializing</param>
        public InspectorSerializer(PsychonautsSettings settings)
        {
            Settings = settings;
            Foldouts = new Dictionary<string, bool>();
            CurrentName = new Stack<string>();
        }

        private const string DefaultName = "<no name>";

        private void DoFoldout(string name, Action action)
        {
            CurrentName.Push(name);

            var fullName = GetFullName(name);

            if (!Foldouts.ContainsKey(fullName))
                Foldouts[fullName] = false;

            Foldouts[fullName] = EditorGUILayout.Foldout(Foldouts[fullName], name ?? DefaultName, true);

            if (Foldouts[fullName])
            {
                Depth++;
                EditorGUI.indentLevel++;

                action();

                Depth--;
                EditorGUI.indentLevel--;
            }

            CurrentName.Pop();
        }

        private void DoWithChangeCheck<T>(Func<T> guiFunc, Action<T> applyAction)
        {
            EditorGUI.BeginChangeCheck();
            T value = guiFunc();
            if (EditorGUI.EndChangeCheck())
                applyAction(value);
        }

        /// <summary>
        /// Serializes a value
        /// </summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="value">The value</param>
        /// <param name="name">The value name, for logging</param>
        /// <returns>The serialized value</returns>
        public T Serialize<T>(T value, string name = null)
        {
            // Get the type
            var type = typeof(T);

            TypeCode typeCode = Type.GetTypeCode(type);

            if (type.IsEnum)
            {
                if (type.GetCustomAttributes(false).OfType<FlagsAttribute>().Any())
                    return (T)(object)EditorGUILayout.EnumFlagsField(name ?? DefaultName, (Enum)(object)value);
                else
                    return (T)(object)EditorGUILayout.EnumPopup(name ?? DefaultName, (Enum)(object)value);
            }

            switch (typeCode)
            {
                case TypeCode.Boolean:
                    return (T)(object)EditorGUILayout.Toggle(name ?? DefaultName, (bool)(object)value);

                case TypeCode.SByte:
                    return (T)(object)(sbyte)EditorGUILayout.IntField(name ?? DefaultName, (sbyte)(object)value);

                case TypeCode.Byte:
                    return (T)(object)(byte)EditorGUILayout.IntField(name ?? DefaultName, (byte)(object)value);

                case TypeCode.Int16:
                    return (T)(object)(short)EditorGUILayout.IntField(name ?? DefaultName, (short)(object)value);

                case TypeCode.UInt16:
                    return (T)(object)(ushort)EditorGUILayout.IntField(name ?? DefaultName, (ushort)(object)value);

                case TypeCode.Int32:
                    return (T)(object)EditorGUILayout.IntField(name ?? DefaultName, (int)(object)value);

                case TypeCode.UInt32:
                    return (T)(object)(uint)EditorGUILayout.LongField(name ?? DefaultName, (uint)(object)value);

                case TypeCode.Int64:
                    return (T)(object)EditorGUILayout.LongField(name ?? DefaultName, (long)(object)value);

                case TypeCode.UInt64:
                    return (T)(object)(ulong)EditorGUILayout.LongField(name ?? DefaultName, (long)(ulong)(object)value);

                case TypeCode.Single:
                    return (T)(object)EditorGUILayout.FloatField(name ?? DefaultName, (float)(object)value);

                case TypeCode.Double:
                    return (T)(object)EditorGUILayout.DoubleField(name ?? DefaultName, (double)(object)value);

                case TypeCode.String:
                    return (T)(object)EditorGUILayout.TextField(name ?? DefaultName, (string)(object)value);

                case TypeCode.Decimal:
                case TypeCode.Char:
                case TypeCode.DateTime:
                case TypeCode.Empty:
                case TypeCode.DBNull:
                case TypeCode.Object:
                    if (type == typeof(UInt24))
                    {
                        return (T)(object)(UInt24)(uint)EditorGUILayout.LongField(name ?? DefaultName, (uint)(UInt24)(object)value);
                    }
                    else if (type == typeof(byte?))
                    {
                        var b = (byte?)(object)value;
                        byte byteValue;
                        bool hasValue = b.HasValue;

                        hasValue = EditorGUILayout.Toggle(name ?? DefaultName, hasValue);

                        if (hasValue)
                            byteValue = (byte)EditorGUILayout.IntField(name ?? DefaultName, b ?? 0);
                        else
                            byteValue = 0;

                        if (hasValue)
                            return (T)(object)(byte?)byteValue;

                        return (T)(object)null;
                    }
                    else
                    {
                        throw new NotSupportedException("The specified generic type is not supported");
                    }
                default:
                    throw new NotSupportedException("The specified generic type is not supported");
            }
        }

        /// <summary>
        /// Serializes the size of an array
        /// </summary>
        /// <typeparam name="T">The array value type</typeparam>
        /// <typeparam name="U">The size value type</typeparam>
        /// <param name="array">The array</param>
        /// <param name="name">The array name, for logging</param>
        /// <returns>The serialized array</returns>
        public T[] SerializeArraySize<T, U>(T[] array, string name = null)
        {
            return array;
        }

        /// <summary>
        /// Serializes an array
        /// </summary>
        /// <typeparam name="T">The array value type</typeparam>
        /// <param name="array">The array</param>
        /// <param name="length">The array length, or -1 to use its current length</param>
        /// <param name="name">The array name, for logging</param>
        /// <returns>The serialized array</returns>
        public T[] SerializeArray<T>(T[] array, long length = -1, string name = null)
        {
            if (length == -1)
                length = array?.Length ?? 0;

            if (array == null)
                array = new T[length];
            else if (length != array.Length)
                Array.Resize(ref array, (int)length);

            DoFoldout($"{name}[{array.Length}]", () =>
            {
                for (int i = 0; i < array.Length; i++)
                    Serialize(array[i], name: $"{name ?? DefaultName}[{i}]");
            });

            return array;
        }

        /// <summary>
        /// Serializes a string of a specified length
        /// </summary>
        /// <param name="value">The string value</param>
        /// <param name="length">The string length</param>
        /// <param name="encoding">The string encoding to use, or null for the default one</param>
        /// <param name="name">The string value name, for logging</param>
        /// <returns>The serialized string value</returns>
        public string SerializeString(string value, long length, Encoding encoding = null, string name = null)
        {
            return EditorGUILayout.TextField(name ?? DefaultName, value);
        }

        /// <summary>
        /// Serializes a serializable object
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="value">The serializable object</param>
        /// <param name="onPreSerialize">Optional action to run before serializing</param>
        /// <param name="name">The object value name, for logging</param>
        /// <returns>The serialized object</returns>
        public T SerializeObject<T>(T value, Action<T> onPreSerialize = null, string name = null)
            where T : IBinarySerializable, new()
        {
            value ??= new T();
            name ??= DefaultName;

            // TODO: Potentially clean this up a bit with all of the casting and maybe move the Psychonuats specific types somewhere else?
            switch (value)
            {
                case Vec2 vec2:
                    DoWithChangeCheck(
                        () => EditorGUILayout.Vector2Field(name, vec2.ToVector2()), 
                        x => value = (T)(object)x.ToVec2());
                    break;

                case Vec3 vec3 when name.Contains("Color"):
                    DoWithChangeCheck(
                        () => EditorGUILayout.ColorField(name, (Vector4)vec3.ToVector3()),
                        x => value = (T)(object)((Vector3)(Vector4)x).ToVec3());
                    break;

                case Vec3 vec3:
                    DoWithChangeCheck(
                        () => EditorGUILayout.Vector3Field(name, vec3.ToVector3()),
                        x => value = (T)(object)x.ToVec3());
                    break;

                case Vec4 vec4 when name.Contains("Color"):
                    DoWithChangeCheck(
                        () => EditorGUILayout.ColorField(name, vec4.ToVector4()),
                        x => value = (T)(object)((Vector4)x).ToVec4());
                    break;

                case Vec4 vec4:
                    DoWithChangeCheck(
                        () => EditorGUILayout.Vector4Field(name, vec4.ToVector4()),
                        x => value = (T)(object)x.ToVec4());
                    break;

                case String1b string1b:
                    string1b.Value = EditorGUILayout.TextField(name, string1b.Value);
                    break;

                case String2b string2b:
                    string2b.Value = EditorGUILayout.TextField(name, string2b.Value);
                    break;

                case String4b string4b:
                    string4b.Value = EditorGUILayout.TextField(name, string4b.Value);
                    break;

                default:
                    DoFoldout(name, () => value.Serialize(this));
                    break;
            }

            return value;
        }

        /// <summary>
        /// Serializes an array of serializable objects
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="array">The serializable object array</param>
        /// <param name="length">The array length, or -1 to use its current length</param>
        /// <param name="onPreSerialize">Optional action to run before serializing each object</param>
        /// <param name="name">The object value name, for logging</param>
        /// <returns>The serialized object array</returns>
        public T[] SerializeObjectArray<T>(T[] array, long length = -1, Action<T, int> onPreSerialize = null, string name = null)
            where T : IBinarySerializable, new()
        {
            if (length == -1)
                length = array?.Length ?? 0;

            if (array == null)
                array = new T[length];
            else if (length != array.Length)
                Array.Resize(ref array, (int)length);

            DoFoldout($"{name}[{array.Length}]", () =>
            {
                for (int i = 0; i < array.Length; i++)
                    SerializeObject(array[i], name: $"{name ?? DefaultName}[{i}]");
            });

            return array;
        }

        /// <summary>
        /// Serializes an object array of undefined size until a specified condition is met
        /// </summary>
        /// <typeparam name="T">The object type</typeparam>
        /// <param name="obj">The object array</param>
        /// <param name="conditionCheckFunc">The condition for ending the array serialization</param>
        /// <param name="getLastObjFunc">If specified the last object when read will be ignored and this will be used to prepend an object when writing</param>
        /// <param name="onPreSerialize">Optional action to run before serializing</param>
        /// <param name="name">The name</param>
        /// <returns>The object array</returns>
        public T[] SerializeObjectArrayUntil<T>(T[] obj, Func<T, int, bool> conditionCheckFunc, Func<T> getLastObjFunc = null,
            Action<T, int> onPreSerialize = null, string name = null)
            where T : IBinarySerializable, new()
        {
            return SerializeObjectArray<T>(obj, obj?.Length ?? 0, onPreSerialize: onPreSerialize, name: name);
        }

        /// <summary>
        /// Serializes bit field values
        /// </summary>
        /// <typeparam name="T">The integer value type</typeparam>
        /// <param name="serializeAction">The serializer action</param>
        public void DoBits<T>(Action<IBitSerializer> serializeAction)
        {
            serializeAction(new InspectorBitSerializer(this));
        }

        /// <summary>
        /// Seeks to the specified stream position
        /// </summary>
        /// <param name="position">The position to go to</param>
        public void GoTo(long position) { }

        private Dictionary<string, bool> Foldouts { get; }
        private Stack<string> CurrentName { get; }
        private string GetFullName(string name) => String.Join(".", CurrentName.Append(name));
        private int Depth { get; set; }

        /// <summary>
        /// The current stream position
        /// </summary>
        public long Position => 0;

        /// <summary>
        /// The current stream length
        /// </summary>
        public long Length => 0;

        /// <summary>
        /// The settings to use when serializing
        /// </summary>
        public PsychonautsSettings Settings { get; }
    }
}