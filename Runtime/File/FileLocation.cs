﻿using System;

namespace PsychoPortal.Unity
{
    [Flags]
    public enum FileLocation
    {
        None = 0,
        FileSystem = 1 << 0,
        Package = 1 << 1,
        Any = FileSystem | Package,
    }
}