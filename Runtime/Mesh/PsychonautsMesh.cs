﻿using System.Linq;

namespace PsychoPortal.Unity
{
    public class PsychonautsMesh
    {
        public PsychonautsMesh(Mesh mesh, PsychonautsMeshFrag[] meshFrags, PsychonautsSkeleton[] skeletons)
        {
            Mesh = mesh;
            MeshFrags = meshFrags;
            Skeletons = skeletons;
        }

        public Mesh Mesh { get; }
        public PsychonautsMeshFrag[] MeshFrags { get; }
        public PsychonautsSkeleton[] Skeletons { get; }

        public JointID GetJointID(string name) => 
            Skeletons.Select(skel => skel.GetJoint(name)).
                Where(joint => joint != null).
                Select(joint => joint.Joint.ID).
                FirstOrDefault();

        public bool CanPlayAnimation(PsychonautsSkelAnim anim)
        {
            JointID rootJointID = GetJointID(anim.RootJointName);

            // Make sure the root joint exists
            if (rootJointID == null)
                return false;

            PsychonautsSkeleton skel = Skeletons[rootJointID.SkeletonIndex];

            // Make sure the amount of joints match
            if (rootJointID.JointIndex == 0 && skel.Joints.Length != anim.JointsCount ||
                rootJointID.JointIndex != 0 && skel.Joints.Length < rootJointID.JointIndex + anim.JointsCount)
                return false;

            // Check blend shape names if there is a blend animation
            if (anim.BlendAnimation?.Channels != null)
            {
                foreach (SharedBlendAnim_Channel channel in anim.BlendAnimation.Channels)
                {
                    if (MeshFrags.All(x => x.MeshFrag.BlendshapeData?.Streams.Any(s => s.BlendMappingName == channel.Name) != true))
                        return false;
                }
            }

            return true;
        }
    }
}