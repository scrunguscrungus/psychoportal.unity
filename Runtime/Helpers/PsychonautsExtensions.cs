﻿using UnityEngine;

namespace PsychoPortal.Unity
{
    public static class PsychonautsExtensions
    {
        // Vector (PsychoPortal -> Unity)
        public static Vector2 ToVector2(this Vec2 v) => new Vector2(v.X, v.Y);
        public static Vector3 ToVector3(this Vec3 v) => new Vector3(v.X, v.Y, v.Z);
        public static Vector4 ToVector4(this Vec4 v) => new Vector4(v.X, v.Y, v.Z, v.W);
        public static Vector3 ToVector3(this NormPacked3 v) => v.ToVec3().ToVector3();

        // Vector (Unity -> PsychoPortal)
        public static Vec2 ToVec2(this Vector2 v) => new Vec2(v.x, v.y);
        public static Vec3 ToVec3(this Vector3 v) => new Vec3(v.x, v.y, v.z);
        public static Vec4 ToVec4(this Vector4 v) => new Vec4(v.x, v.y, v.z, v.w);
        //public static NormPacked3 ToNormPacked3(this Vector3 v) => ; // TODO: Implement

        // Quaternion (PsychoPortal -> Unity)
        public static Quaternion ToQuaternionDeg(this Vec3 v) => Quaternion.Euler(0, 0, v.Z) * 
                                                                 Quaternion.Euler(0, v.Y, 0) * 
                                                                 Quaternion.Euler(v.X, 0, 0);
        public static Quaternion ToQuaternionRad(this Vec3 v) => Quaternion.Euler(0, 0, v.Z * Mathf.Rad2Deg) * 
                                                                 Quaternion.Euler(0, v.Y * Mathf.Rad2Deg, 0) * 
                                                                 Quaternion.Euler(v.X * Mathf.Rad2Deg, 0, 0);
        public static Quaternion ToQuaternion(this Quat q) => new Quaternion(q.X, q.Y, q.Z, q.W); 
        public static Quaternion ToQuaternion(this CompQuat q) => q.ToQuat().ToQuaternion();
        public static Quaternion ToQuaternion(this PS2_CompQuat q) => q.ToQuat().ToQuaternion();

        // Color (PsychoPortal -> Unity)
        public static Color ToColor(this BGRA8888Color c, float factor = 1f) => 
            new Color(
                c.Red / 255f * factor, 
                c.Green / 255f * factor, 
                c.Blue / 255f * factor, 
                c.Alpha / 255f);
    }
}